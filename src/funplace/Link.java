/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import eduni.simjava.Sim_system;
import java.util.logging.Logger;
import org.jgrapht.*;
import org.jgrapht.graph.*;

/**
 *
 * @author anix- represents an undirected link in the network
 */
public class Link {

    Network net;
    Node from, to;
    Port fromPort, toPort;

    SimpleGraph<Node, Link> graph;

    String name;

    private int THROUGHPUT_BYTES_PER_SEC = 1250000000; // throughput of the link in Bytes/Sec
    private int usage; // usage of throughput in number of bytes
    private int msgCount; // number of messages sent over link

    public Link(Node from, Node to) {

        super();

//        this.net = Network.getInstance();
        if (from.getNodeName().compareTo(to.getNodeName()) < 0) {
            this.name = "L-" + from.getNodeName() + "-" + to.getNodeName();
        } else {
            this.name = "L-" + to.getNodeName() + "-" + from.getNodeName();
        }

        info("Creating link " + this.name);
        this.from = from;
        this.to = to;
        fromPort = from.addNetworkLink(to, this);
        toPort = to.addNetworkLink(from, this);

        usage = 0;
        msgCount = 0;

//        System.out.println("Linking ports " + from.getNodeName() + ":" + fromPort.getPortName() +
//                " and " + to.getNodeName() + ":" + toPort.getPortName());
//        
//        System.out.println(Sim_system.running()? "RUNNING" : "NOT RUNNING");
        Sim_system.link_ports(from.getNodeName(), fromPort.getPortName(),
                to.getNodeName(), toPort.getPortName());

    }

    public Port getFromPort() {
        return fromPort;
    }

    public Port getToPort() {
        return toPort;
    }

    public Node getFrom() {
        return from;
    }

    public Port getPortOnNode(Node node) {
        assert (node.equals(getFrom()) || node.equals(getTo()));

        if (node.equals(getFrom())) {
            return fromPort;
        } else {
            return toPort;
        }
    }

    public Node getTo() {
        return to;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    protected Node getSource() {
        return from;
    }

    protected Node getTarget() {
        return to;
    }

    protected void setDebugAtPort(Node n) {
        Port p = getPortOnNode(n);
        p.setDebugFlag(true);
    }

    private void info(String msg) {
        Network.info(msg);
    }

    public void updateUsage(int usageBytes) {
//        System.out.println("Usage for link " + toString() + " updated by "
//                + usageBytes + " bytes");
        this.msgCount++;
        this.usage += usageBytes;
    }

    public int getUsage() {
        return usage;
    }

    public int getMsgCount() {
        return msgCount;
    }

    // return the usage of this link bandwidth as parts-per-million of total bandwidth for the given duration
    public double getBwUsage(int msecs) {
        if(usage == 0) {
            return 0;
        }
        
        if(msecs < 0) System.out.println("getBwUsage - negative time");
        return ((usage/ (msecs * 0.5 * THROUGHPUT_BYTES_PER_SEC))* 1000000);
    }
}
