/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import eduni.simjava.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.*;
import org.jgrapht.graph.*;

/**
 *
 * @author anix
 */
public class Node extends Sim_entity {

    int nodeId; // ID of the node

    protected static final int DEFAULT_CAPACITY = 200;
    private final static Logger logger = Logger.getLogger(Node.class.getName());

    Controller controller;
    Network net;
    SimpleGraph<Node, Link> graph;

    public static class Type {

        static final int OTHER = 0;
        static final int TOR = 1;
        static final int AGGR = 2;
        static final int CORE = 3;
        static final int HYPER = 4;
    };

    public static final String NODE_TYPE[] = {"OTHER", "TOR", "AGGR", "CORE"};

    String name; // name of the node
    int capacity;
    int type;

    Map<Integer, Flow> flows; // a map of flows indexed by flow ID
    Map<Integer, FlowNode> flowNodes; // a map of flow nodes indexed by flow ID
    Map<Integer, VNFNode> vnfNodes; // VNF instances on this node indexed
    Map<Integer, VNFNode> vnfNodesNetPack;
    Map<Integer, VNFNode> vnfNodesHeuristicA;
    // by VNF id

    Map<Integer, Port> neighborPorts; // ports indexed by neighbor node ID
    Map<Integer, Port> ports; // ports indexed by port ID
    Map<Integer, Node> neighbors; // neighbors indexed by node ID

    // A holder for all outgoing messages to be sent out
    // at the end of each round
    List<Message> outgoingMessages;
    List<FlowNode> paths;

    // X & Y co-ordinates of the node 
    private int xCoord;
    private int yCoord;

    private static final Sim_predicate triggerPred = new Sim_type_p(Message.Type.TRIGGER);
    private static final Sim_predicate requestPred = new Sim_type_p(Message.Type.REQUEST);
    private static final Sim_predicate offerPred = new Sim_type_p(Message.Type.OFFER);
    private static final Sim_predicate confirmPred = new Sim_type_p(Message.Type.CONFIRM);

    final double LOAD_THRESHOLD_FACTOR = 0.7;

    boolean debug;

    public Map<Integer, Port> getPorts() {
        return ports;
    }

    public Node(Network net, SimpleGraph<Node, Link> graph,
            int nodeId, String name, int capacity, int type) {
        super(name);
        this.net = net;
        this.graph = graph;
        this.nodeId = nodeId;
        this.name = name;
        this.type = type;

        logger.setLevel(Network.defaultLogLevel);
        this.debug = false;

        this.capacity = capacity;
        this.outgoingMessages = new ArrayList<>();
        this.paths = new ArrayList<>();

        this.ports = new HashMap<>();
        this.flowNodes = new HashMap<>();
        this.vnfNodes = new HashMap<>();
        this.vnfNodesNetPack = new HashMap<>();
        this.vnfNodesHeuristicA = new HashMap<>();

        // initialize X and Y coordinates
        this.xCoord = 0;
        this.yCoord = 0;

        // create the HOST port for this node
        Port hostPort = new Port(this, Port.HOST_PORT_ID);
        this.ports.put(Port.HOST_PORT_ID, hostPort);

        // create VNF nodes for all known VNFs in this node
        createAllVnfNodes();

    }

    public Node(Network net, SimpleGraph<Node, Link> graph,
            int nodeId, String name, int capacity) {
        this(net, graph, nodeId, name, capacity, Type.OTHER);
    }

    private void createAllVnfNodes() {
        Collection<VNF> vnfs = VNF.getVnfList();
        VNFNode vnode;

        for (VNF vnf : vnfs) {
            // create the VNF node

            vnode = new VNFNode(this, vnf);

            // insert the VNF node in this node
            vnfNodes.put(vnf.getVnfId(), vnode);
        }
    }

    public int getNodeId() {
        return nodeId;
    }

    public SimpleGraph<Node, Link> getGraph() {
        return graph;
    }

    public String getNodeName() {
        return name;
    }

    public String toString() {
        return name;
    }

    public void body() {

        double round;

        while (Sim_system.running()) {

//            triggerCount = sim_waiting(triggerPred);
//
//            if (triggerCount == 0) {
//                // wait till you receive a trigger
//            }
//            offerCount = sim_waiting(offerPred);
//            info("Trigger received by Node " + toString() + "; Trigger Count "
//                    + triggerCount);
            Sim_event e = new Sim_event();
            sim_get_next(triggerPred, e);

            round = Network.clock();

            info("Trigger event received at time " + round);

//            if (round >= net.endRound - 5) {
//                sim_completed(e);
//                sim_pause(1.0);
//                continue;
//            }
//            if(e.event_time() >= net.endRound - 5) { 
//                break;
////                sim_pause(1.0);
////                continue; // don't process events too close to end round
//            }
            // processing a round 
            info("Processing a round at node " + getNodeName()
                    + " at time " + e.event_time());

            // first process the Transfer Offers
            processTransferOffersOrConfirmations(true);

            // then process all the transfer confirmations
            processTransferOffersOrConfirmations(false);

            // first, update the max utility values for all the VNF nodes
            updateMaxUtilities();

            // now, process all the incoming transfer requests
            processTransferRequests();

            if (e.event_time() < (net.maxRounds - 1)) {
                //  if this is NOT the last round, 
                // schedule Transfer Requests to all neighbors
                for (VNFNode vnfNode : vnfNodes.values()) {
                    vnfNode.generateTransferRequests();
                }
            }

            // Process the trigger event
            sim_process(0.0);

            // The event has been serviced
            sim_completed(e);

//            sim_pause(1.0); // let's try this!
//            if (e.get_tag() == Message.Type.REQUEST) {
//                // TBD- info a message to indicate that TR was received 
//                info("TR received by Node " + getNodeName() + " from "
//                        + ((Node) e.get_data()).getNodeName() + " at time "
//                        + e.event_time());
//                continue;
//            }
        }
    }

    private void processTransferOffersOrConfirmations(boolean offer) {

        int msgCount;

        if (offer) {
            msgCount = sim_waiting(offerPred);
        } else {
            msgCount = sim_waiting(confirmPred);
        }

        if (msgCount > 0) {
            info(msgCount + (offer ? " OFFER(s) " : " CONFIRM(s) ") + "received at Node " + toString()
                    + " at time " + Network.clock());
        }

        for (int i = 0; i < msgCount; i++) {
            Sim_event e = new Sim_event();
            if (offer) {
                sim_get_next(offerPred, e);
            } else {
                sim_get_next(confirmPred, e);
            }

            Message msg = (Message) e.get_data();

            handleTransferOfferOrConfirm(msg, offer);

            sim_completed(e);
        }
    }

    private boolean isPastEndRound() {
        double round = net.clock();

        if (round >= net.endRound - 5) {
            return true;
        } else {
            return false;
        }

    }

    private void handleTransferOfferOrConfirm(Message msg, boolean offer) {

        boolean pull = true; // TBD
        boolean offersCrossedOnWire = false;

        if (isPastEndRound()) {
            // don't do any processing if it's close to the end of simulation
            return;
        }

        Port rxPort = msg.link.getPortOnNode(this);

        assert (rxPort != null);

        if (msg.dstNodeId != getNodeId()) {
            Network.info("FORWARDing TO/TC " + msg.toString() + " at " + toString());
            // this OFFER or CONFIRM is not meant for this local node
            // follow the flows in the message and forward it either
            // upstream or downstream
            forwardTransferOfferOrConfirm(msg, rxPort, offer);

            return;
        }

        VNFNode vnfNode = (VNFNode) vnfNodes.get(msg.vnf.getVnfId());

        //INFO
        warning((offer ? "OFFER" : "CONFIRM") + " received by Node "
                + toString() + " from N"
                + msg.srcNodeId + " for "
                + vnfNode.toString() + " at time "
                + Network.clock());
        if (pull) {
            if (offer) {
                // TRANSFER OFFER
                offersCrossedOnWire = rxPort.pullVnf(vnfNode);
            } else {
                // TRANSFER CONFIRM
                rxPort.pushVnf(vnfNode);

                // update the Network with this round
                net.updateNumRounds();
            }
        } else {
            // have the port de-link all the flows
            // from the VNF node to essentially transfer the
            // VNF to the requesting node
        }

        if (offer && !offersCrossedOnWire) {
            // Schedule a CONFIRM message for the neighbor
            rxPort.generateTransferOfferOrConfirm(msg, false);
        }

    }

    private void forwardTransferOfferOrConfirm(Message msg, Port rxPort, boolean offer) {

        Port txPort;
        FlowNode fnode;
        // determine the port on which this msg should be forwarded

        Flow flow = msg.getFlowGroup().getFlows().get(0); // get the first flow
        fnode = getFlowNodeByFlowId(flow.getFlowId());

        if (fnode.ingressPort.equals(rxPort)) {
            txPort = fnode.egressPort;
            msg.link = txPort.getLink();
        } else {
            txPort = fnode.ingressPort;
            msg.link = txPort.getLink();
        }

        txPort.forwardTransferOfferOrConfirm(msg);

        // since a VNF is being pushed or pulled "over" this node
        // the previous / next markers must be moved left or right
        // depending on the direction of the transfer  
        if (!offer) {  // CONFIRM
            for (Flow f : msg.getFlowGroup().getFlows()) {
                fnode = getFlowNodeByFlowId(f.getFlowId());

                if (fnode == null) {
                    // CHECK THIS!
                    // something is wrong
                    continue;
                }

                if (fnode.ingressPort.equals(rxPort)) {
                    // Based on the direction of the CONFIRM
                    // VNF is being moved upstream
                    // move the markers downstream
                    fnode.moveSfcMarkersDownstream();

                    Network.info("Moving SFC markers downstream for " + fnode.toString());
                } else {
                    fnode.moveSfcMarkersUpstream();

                    Network.info("Moving SFC markers upstream for " + fnode.toString());
                }

            }
        }
    }

    private void processTransferRequests() {

        int requestCount;

        if (!Sim_system.running()) {
            return;
        }

        // check for the transfer requests currently waiting
        requestCount = sim_waiting(requestPred);

        info(requestCount + " REQUESTs received at Node " + toString());

        // handle all the incoming transfer requests
        // by calling handleTransferRequest() on each
        // first, clear all the outgoing transfer requests on all ports
//        for (Port p : ports.values()) {
//            p.resetTRQueue();
//        }
        // clear are residual forces in the flow nodes from previous rounds
        for (FlowNode fn : flowNodes.values()) {
            if (!fn.usOfferPending) {
                fn.usVnf = null;
                fn.usDifferential = 0;
                fn.usMaxUtility = 0;
                fn.usDiffNodeId = 0;
            }

            if (!fn.dsOfferPending) {
                fn.dsVnf = null;
                fn.dsDifferential = 0;
                fn.dsMaxUtility = 0;
                fn.dsDiffNodeId = 0;
            }
        }

        for (int i = 0; i < requestCount; i++) {
            Sim_event e = new Sim_event();
            sim_get_next(requestPred, e);

            Message msg = (Message) e.get_data();
            int srcNodeId = msg.srcNodeId;

            info("REQUEST received by Node " + toString() + " from "
                    + srcNodeId + " at time "
                    + e.event_time());
            handleTransferRequest((Message) e.get_data());

            sim_completed(e);
        }
    }

    public void updateMaxUtilities() {

        for (VNFNode vnfNode : vnfNodes.values()) {

            // calculate the max utility for this VNF node
            vnfNode.updateMaxUtility();
        }

        // take the list of VNF nodes and sort them based on max utility
        List vnfNodesList = new ArrayList(vnfNodes.values());
        Comparator vnfNodeComparator = new VNFNode.VNFNodeComparator();
        vnfNodesList.sort(vnfNodeComparator);

        //TBD temporary code to allow all TR's to be generated
        // FIX LATER
//         capacity = 100000;
        //Find the target increase in utility for this node
//        int targetIncrease = capacity - totalLoad;
//         int targetIncrease = getRemainingCapacity(Network.Heuristic.DANCE);
        int targetIncrease = 100000;

        // iterate the VNF nodes in decreasing order of differential
        // and generate pull requests
        for (VNFNode vnfNode : vnfNodes.values()) {

            if (vnfNode.getDifferential() < targetIncrease) {
                targetIncrease -= vnfNode.getDifferential();
            } else {
                // reset max utility to avoid pull requests for this VNF
                vnfNode.resetMaxUtility();
            }

        }

    }

    public Port addNetworkLink(Node neighbor, Link link) {
        // get the next available port ID
        int portId = ports.values().size();

        Port port = new Port(this, neighbor, portId, link);
        ports.put(portId, port);

        return port;

    }

    public void addVnfNode(Flow flow, VNF vnf, int method) {
        VNFNode vnode;

        Map<Integer, VNFNode> vnfNodeMap;

        if (method == Network.Heuristic.DANCE) {
            vnfNodeMap = vnfNodes;
        } else if (method == Network.Heuristic.NETPACK) {
            vnfNodeMap = vnfNodesNetPack;
        } else {
            vnfNodeMap = vnfNodesHeuristicA;
        }

        // see if a VNF Node exists already for this VNF
        vnode = getVnfNode(vnf.getVnfId(), method);
        if (vnode == null) {
            // create the VNF node

            vnode = new VNFNode(this, vnf);

            // insert the VNF node in this node
            vnfNodeMap.put(vnf.getVnfId(), vnode);
        }

        info("Adding VNF Node " + vnode.toString() + " for flow " + flow.toString());

        // add the flow to the VNF Node
        vnode.addFlow(flow);
    }

    public void removeVnfNode(Flow flow, VNF vnf, int method) {
        VNFNode vnode;

//        Map<Integer, VNFNode> vnfNodeMap;
//
//        if (method == Network.Heuristic.DANCE) {
//            vnfNodeMap = vnfNodes;
//        } else if (method == Network.Heuristic.NETPACK) {
//            vnfNodeMap = vnfNodesNetPack;
//        } else {
//            vnfNodeMap = vnfNodesHeuristicA;
//        }
        // get the VNF Node for this VNF
        vnode = getVnfNode(vnf.getVnfId(), method);

        info("Removing VNF Node " + vnode.toString() + " for flow " + flow.toString());

        // add the flow to the VNF Node
        vnode.removeFlow(flow);
    }

    private VNFNode getVnfNode(int vnfId, int method) {

        Map<Integer, VNFNode> vnfNodeMap;

        if (method == Network.Heuristic.DANCE) {
            vnfNodeMap = vnfNodes;
        } else if (method == Network.Heuristic.NETPACK) {
            vnfNodeMap = vnfNodesNetPack;
        } else {
            vnfNodeMap = vnfNodesHeuristicA;
        }

        return vnfNodeMap.get(vnfId);
    }

    protected int getVNFUtility(VNF vnf, int method) {
        VNFNode vnode = getVnfNode(vnf.getVnfId(), method);
        if (vnode == null) {
            return 0;
        }

        return vnode.getCurrentUtility();
    }

    private Collection getAllPorts() {
        return this.ports.values();
    }

    private Port getPortFromId(int portId) {
        return (Port) ports.get(portId);
    }

    public Port getHostPort() {
        return (Port) ports.get(Port.HOST_PORT_ID);
    }

    public FlowNode getFlowNodeByFlowId(int flowId) {
        return (FlowNode) flowNodes.get(flowId);
    }

    // handle a single Transfer Request msg from a neighbor
    public void handleTransferRequest(Message msg) {

        info("Handle Request " + msg.toString());
        boolean forwardedRequest = false;

        if (isPastEndRound()) {
            // don't do any processing if it's close to the end of simulation
            return;
        }

        Port rxPort = msg.link.getPortOnNode(this);

        assert (rxPort != null);

        forwardedRequest = (msg.srcNodeId != rxPort.neighbor.getNodeId());
        if (forwardedRequest) {
            Network.info("Received a FORWARDED Transfer Request at "
                    + toString() + "; " + msg.toString());
        }

        if (msg.maxUtility == 0) {
            warning("Received " + (forwardedRequest ? "FORWARDED " : "") + "TR with zero max utility");
            return;
        }

        // if the transfer request has target utility more than local VNF
        // node, then schedule a Trasnfer Offer. First, find the local
        // VNF node for the VNF of this TR
        VNFNode vnfNode = (VNFNode) vnfNodes.get(msg.vnf.getVnfId());

        //SADHANA
        double localMaxUtility;
        double msgMaxUtility;

        if (vnfNode.currentUtility > 0) {
            // there is a VNF instance hosted locally
            info("Local utility is non-zero");

            //SADHANA
            localMaxUtility = vnfNode.maxUtility;

            // offset the received max utility by the cost of migrating a VNF
            msgMaxUtility = msg.maxUtility - net.getVnfMigrationCost();

            if ((localMaxUtility < msgMaxUtility)
                    || ((localMaxUtility == msgMaxUtility)
                    && (vnfNode.getDifferential() > msg.differential))
                    || ((localMaxUtility == msgMaxUtility)
                    && (vnfNode.getDifferential() == msg.differential)
                    && (getNodeId() > msg.srcNodeId))) {

                // Commented out for SADHANA
//            if ((vnfNode.maxUtility < msg.maxUtility)
//                    || ((vnfNode.maxUtility == msg.maxUtility)
//                    && (vnfNode.getDifferential() > msg.differential))
//                    || ((vnfNode.maxUtility == msg.maxUtility)
//                    && (vnfNode.getDifferential() == msg.differential)
//                    && (getNodeId() > msg.srcNodeId))) {
                // either the requesting node has higher max utility for 
                // the VNF (OR) 
                // it has the same max utility, but smaller differential (tiebreaker 1)
                // (OR)
                // above two parameters are same, but the requesting node
                // has a smaller node ID (tiebreaker 2)
                // we must also sanity check to make sure that the
                // VNF that is being requested for transfer has the
                // flow(s) in suitable states for transferring the VNF
                if (!rxPort.sanityCheckTransferRequest(msg)) {
                    warning("Sanity check failed for TR " + msg.toString()
                            + " by " + toString() + " at " + Network.clock());
                    return;
                }

                info("Scheduling a Transfer Offer for " + vnfNode.toString()
                        + " to N" + msg.srcNodeId + " at time "
                        + Network.clock() + "; local utility = "
                        + vnfNode.currentUtility + "; neighbor's max utility = "
                        + msg.maxUtility);

                rxPort.generateTransferOfferOrConfirm(msg, true);
                return;
            } else {
                // local VNF node has higher utility than the TR 
                // ignore the request
                info("Local utility higher than in the request- ignoring");
                return;
            }

        }

        rxPort.transferForce(msg);

    }

//    /*
//     * generatePushRequests OLD- this method is called if the local load
//     * is more than the capacity. Offload one NF instance at a time in as 
//     * many flow paths are needed to bring the load down below the capacity
//     */
//    private void generatePushRequests() {
//
//        // "push" some of the load upstream or downstream
//        int targetSavings = totalLoad - capacity;
//        int savings = 0;
//
//        for (FlowNode path : paths) {
//            // reduce the load along this path and calculate the savings
//            savings += path.reduceLoad(targetSavings - savings);
//            if (savings >= targetSavings) {
//                break;
//            }
//        }
//    }
//
//    private void generatePullRequests(int currentLoad, double threshold) {
//
//        int loadIncrease = 0;
//
//        for (FlowNode path : paths) {
//            loadIncrease += path.generatePull();
//
//            if (currentLoad + loadIncrease > threshold) {
//                // we have crossed the threshold
//                break;
//            }
//        }
//    }
    public void addOutgoingMessage(Message msg) {
        outgoingMessages.add(msg);
    }

    public void addFlowNode(Flow flow, Port ingress, Port egress) {

        FlowNode fnode = new FlowNode(this, flow, ingress, egress);
        flowNodes.put(flow.getFlowId(), fnode);

        info("Adding Flow Node Flow ID " + flow.getFlowId() + " at Node " + toString() + "; Ingress "
                + ingress.getPortId() + "; Egress " + egress.getPortId());

        // add the flow to the ingress and egress ports
        ingress.addFlow(flow, true);
        egress.addFlow(flow, false);

    }

    public void utFlowNodeCheck(Flow f, int vnfId) {
        FlowNode fnode = getFlowNodeByFlowId(f.getFlowId());

        fnode.utCheckVnf(VNF.getVNFbyId(vnfId));
    }

    // getLoad- get the load on this node from the VNF instances
    public int getLoad() {

        int load = 0;

        for (VNFNode vnode : vnfNodes.values()) {

            if (vnode.currentUtility > 0) {
                load += vnode.getVnf().staticCost
                        + (vnode.getVnf().dynamicCost * vnode.currentUtility);
            }
        }

        return load;
    }

    public int getVnfInstanceCount(int method) {
        int count = 0;

        Map<Integer, VNFNode> vnfNodeMap;

        if (method == Network.Heuristic.DANCE) {
            vnfNodeMap = vnfNodes;
        } else if (method == Network.Heuristic.NETPACK) {
            vnfNodeMap = vnfNodesNetPack;
        } else {
            vnfNodeMap = vnfNodesHeuristicA;
        }

        for (VNFNode vnode : vnfNodeMap.values()) {

            if (vnode.currentUtility > 0) {
                count += 1;
            }
        }

        return count;

    }

    public int getVnfFlowCount(int method) {
        int count = 0;
        Map<Integer, VNFNode> vnfNodeMap;

        if (method == Network.Heuristic.DANCE) {
            vnfNodeMap = vnfNodes;
        } else if (method == Network.Heuristic.NETPACK) {
            vnfNodeMap = vnfNodesNetPack;
        } else {
            vnfNodeMap = vnfNodesHeuristicA;
        }

        for (VNFNode vnode : vnfNodeMap.values()) {
            count += vnode.flows.size();
        }

        return count;
    }

    public int getRemainingCapacity(int method) {

        return (capacity - getVnfFlowCount(method));
    }

    public void setDebugFlag(boolean val) {
        this.debug = val;
    }

    public void info(String msg) {
        if (logger.isLoggable(Level.INFO) || debug) {
            System.out.println(toString() + " " + msg);
        }
    }

    public void warning(String msg) {
        if (logger.isLoggable(Level.WARNING)) {
            System.out.println("WARNING: " + msg);
        }
    }

    // getters and setters for X & Y coordinates
    public int getxCoord() {
        return xCoord;
    }

    public int getyCoord() {
        return yCoord;
    }

    public void setxCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    public void setyCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    public void prettyPrint() {
        System.out.print("Node: " + name + " " + "; LOAD = " + getLoad() + " ");
        System.out.print("Neighbors :: ");
        for (Port p : ports.values()) {
            p.prettyPrint();
        }

        System.out.println();
    }

    @Override
    public void sim_schedule(Sim_port port, double delay, int tag, Object data) {
        double round = Network.clock();

        if (round >= net.endRound - 5) {
            return;
        }

        if (!Sim_system.running()) {
            return;
        }

        try {
            super.sim_schedule(port, delay, tag, data);
        } catch (java.lang.NullPointerException e) {
            System.out.println("NPE: Round = " + round);
            throw e;
        }

    }

}
