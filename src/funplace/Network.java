/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import eduni.simjava.Sim_entity;
import eduni.simjava.Sim_event;
import eduni.simjava.Sim_predicate;
import eduni.simjava.Sim_system;
import eduni.simjava.Sim_type_p;
import java.util.logging.Level;
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.alg.shortestpath.*;
import java.io.PrintWriter;
import java.io.IOException;

/**
 *
 * @author anix
 */
public class Network extends Sim_entity {

    protected class Heuristic {

        public static final int DANCE = 0;
        public static final int NETPACK = 1;
        public static final int HEURISTIC_A = 2;

        public static final int NUM_HEURISTICS = 3;
    }

    String name;
    String topo;
    private final int numFlows;
//    String outputDir;

    // Link Usage statistics
    private double avgLinkUsage;
    private int totalLinkUsage;
    private int maxLinkUsage;
    private double avgMsgCount;
    private int totalMsgCount;
    private double avgBwUsage;
    private double maxBwUsage;

    Map<Integer, Node> nodes; // map of nodes indexed by node ID string

    SimpleGraph<Node, Link> graph;
    LinkFactory linkFactory;
    DijkstraShortestPath<Node, Link> dsp;
    KShortestPaths<Node, Link> ksp;

    Map<Integer, Flow> flows;

    PrintWriter writer, xyWriter;

    double numRounds = 0.0;

    private final static Logger logger = Logger.getLogger(Network.class.getName());
    protected final static Level defaultLogLevel = Level.SEVERE;

//    private final Sim_predicate triggerPred = new Sim_type_p(Message.Type.TRIGGER);
    public static int MAX_ROUNDS = 300;

    // interval (in rounds) between additional flow provisioning steps
    // this is applicable for the dynamic case
    public static final int ADDNL_FLOWS_INTERVAL = 50;

    // For the dynamic case, the interval between successive performance reports
    public static final int OPT_REPORT_INTERVAL = 10;

    // Flag to run dynamic provisioning of flows
    public static final int IS_DYNAMIC = 0;

    public static int KSHORTEST_K = 3;
    public static boolean KSHORTEST_ACTIVE = true;

    // SADHANA
    public final int MSECS_PER_ROUND = 500; // estimated time per round of simulation
    public double vnfMigrationCost; // cost of migrating a VNF in units of # of VNFs.
    public double fdCoeff; // coefficient for distribution of flow sizes
                       // 0.9 indicates 10% of the flows will contrbute 90% of the data rate

    private NetworkTest networkTest;
    public int unitTestId = 0;
    public int numVnfInstances = 0;
    public int deficitCost = 0;
    public int maxRounds; // don't run beyond this limit
    protected double startRound, endRound; // round when this nework started
    protected boolean sfcHetero;
    protected int sfcLen;
    private boolean ecmp;
//    private int initialLoad; // initial load before running the algorithm
    private int initialVnfInstances;
    private int netPackVnfInstances;
    private int heuristicAVnfInstances;
    private int initialVnfFlowCount;

    // Result variables
    int vnfInstances = 0;
    int vnfFlowCount = 0;

    int ftTopoK;
    int ftTopoH;

    public Network(String netName, String topo, int ftTopoK, int ftTopoH,
            String outputDir, int numFlows, int startRound, int endRound,
            int sfcLen, boolean sfcHetero, boolean ecmp, double vnfMigrationCost,
            double fdCoeff) {
        super(netName);

        this.name = netName;
        this.topo = topo;
        this.numFlows = numFlows;
        this.maxRounds = MAX_ROUNDS;
        this.startRound = startRound;
        this.endRound = endRound;
        this.sfcLen = sfcLen;
        this.sfcHetero = sfcHetero;
        this.ecmp = ecmp;
        //SADHANA
        this.vnfMigrationCost = vnfMigrationCost;
        this.fdCoeff = fdCoeff;
        
        this.ftTopoK = ftTopoK;
        this.ftTopoH = ftTopoH;

        nodes = new HashMap<>();
        flows = new HashMap<>();
        linkFactory = new LinkFactory();

        graph = new SimpleGraph<>(linkFactory);

        try {
            writer = new PrintWriter(outputDir
                    + this.name + ".dat", "ASCII");

            xyWriter = new PrintWriter(outputDir
                    + this.name + ".csv", "ASCII");

        } catch (IOException e) {
            warning("Unable to open dump file for writing");
            return;
        }

        logger.setLevel(defaultLogLevel);

//        this.initialLoad = 0;
        this.initialVnfInstances = 0;
        this.initialVnfFlowCount = 0;
        this.netPackVnfInstances = 0;
        this.heuristicAVnfInstances = 0;

        avgLinkUsage = 0.0;
        totalLinkUsage = 0;
        maxLinkUsage = 0;
        avgMsgCount = 0;
        totalMsgCount = 0;
        avgBwUsage = 0;
        maxBwUsage = 0;

        networkTest = new NetworkTest(this, topo, ftTopoK, ftTopoH, numFlows);
        networkTest.addTopoAndInitialFlows();

//        networkTest.addAdditionalFlows(numFlows);
        for (Node n : nodes.values()) {
//            initialLoad += n.getLoad();
            initialVnfInstances += n.getVnfInstanceCount(Heuristic.DANCE);
            initialVnfFlowCount += n.getVnfFlowCount(Heuristic.DANCE);
            netPackVnfInstances += n.getVnfInstanceCount(Heuristic.NETPACK);
            heuristicAVnfInstances += n.getVnfInstanceCount(Heuristic.HEURISTIC_A);

        }

        System.out.println("Created network " + netName + " with start round = "
                + startRound + " and end round = " + endRound +
                " ; VNF Migration Cost = " + vnfMigrationCost +
                " ; Flow Dist. Coeff = " + fdCoeff);
    }

    public Map<Integer, Node> getNodes() {
        return nodes;
    }

    public int getNumFlows() {
        return numFlows;
    }

    private double getRoundsTaken() {
        return numRounds - startRound;
    }
    
    public double getVnfMigrationCost() {
        return vnfMigrationCost;
    }
    
    public double getFlowDistributionCoefficient() {
        return fdCoeff;
    }

    public SimpleGraph<Node, Link> getGraph() {
        return graph;
    }

    protected void createDsp() {
        dsp = new DijkstraShortestPath(graph);
    }

    protected void createKsp() {
        ksp = new KShortestPaths(graph, KSHORTEST_K);
    }

    public void updateNumRounds() {
        numRounds = clock();
    }

    private void dump(String str) {
        writer.print(str);
    }

    private void dumpln(String str) {
        writer.println(str);
    }

    private void dumpxy(Node node) {
        // dump X Y coordinates and VNF instances
        xyWriter.println(node.toString() + ","
                + node.getxCoord() + ","
                + node.getyCoord() + ","
                + node.getVnfFlowCount(Heuristic.DANCE));
    }

    // dump out the network- including nodes, flows and VNFs
    // in order to feed as input to the IP solver
    private void dumpInfo() {

        String nodeString;
        String vnfString;
        int roundsTaken = (int) numRounds - (int) startRound;

        dumpln("# STATS " + roundsTaken + " " + initialVnfInstances + " " + vnfInstances
                + " " + netPackVnfInstances + " " + heuristicAVnfInstances);

        dumpln("# Rounds taken = " + getRoundsTaken()); // + ","
        dumpln("# " + topo + " Topology K = " + ftTopoK + "; H = " + ftTopoH);
        dumpln("# SFC Len = " + sfcLen + ": "
                + (sfcHetero ? "HETERO" : "HOMO"));
        dumpln("# Num of Flows = " + numFlows);
        dumpln("# Num of VNF instances = " + initialVnfInstances + " (Initial) "
                + vnfInstances + " (Final)");
        dumpln("# Num of VNF flow instances = " + initialVnfFlowCount + " (Initial) "
                + vnfFlowCount + " (Final)");
        dumpln("# Num of nodes = " + nodes.size() + "; Num of Links = " + graph.edgeSet().size());
        dumpln("# USAGE " + totalLinkUsage + " " + maxLinkUsage + " " + avgLinkUsage
                + " " + totalMsgCount + " " + avgMsgCount + " " + maxBwUsage + " " + avgBwUsage + " " + getRoundsTaken() * MSECS_PER_ROUND);
        dumpln("# Total Link Usage = " + totalLinkUsage + "; Max Link Usage " + maxLinkUsage
                + "; Avg Link Usage = " + avgLinkUsage);
        dumpln("# Total Msg Count = " + totalMsgCount + "; Avg Msg Count = " + avgMsgCount);
        dumpln("# Max Bw Usage = " + maxBwUsage + "; Avg Bw Usage = " + avgBwUsage);

        dumpln("");
        dumpln("");
        dumpln("data;"); // enter AMPL data mode
        dumpln("");

        dump("set NETWORK := " + name);
        dumpln(";");
        dumpln("");
        
        // SADHANA- temporarily disable dumping out the stuff
        // needed for AMPL. This saves some time. 
        return;

        // next, dump out the nodes
//        dump("set NODE :=");
//        nodeString = "";
//        for (Node n : nodes.values()) {
//            nodeString += (" " + n.name);
//            dumpxy(n);
//        }
//
//        dumpln(nodeString + ";");
//        dumpln("");
//
//        // next, dump out the VNFs
//        dump("set VNF :=");
//        vnfString = "";
//        for (VNF v : VNF.getVnfList()) {
//            vnfString += (" " + v.vnfName);
//        }
//        dumpln(vnfString + ";");
//        dumpln("");
//
//        dump("set FLOW :=");
//        for (Flow f : flows.values()) {
//            dump(" " + f.toString());
//        }
//        dumpln(";");
//        dumpln("");
//
//        // Now dump the parameters
//        // first, dump the network parameter
//        dump("param net := ");
//        dumpln(name + " 1;");
//        dumpln("");
//
//        // next, capacities of the nodes
//        dump("param capacity :=");
//        for (Node n : nodes.values()) {
//            dump(" " + n.name + " " + n.capacity);
//        }
//        dumpln(";");
//        dumpln("");
//
//
//        // now, dump the path parameter
//        dump("param path: " + nodeString + ":= ");
//        List<Node> path;
//        int index;
//
//        for (Flow f : flows.values()) {
//            path = f.getPath().getVertexList();
//
//            dumpln("");
//
//            dump(f.toString());
//
//            for (Node n : nodes.values()) {
//                index = path.indexOf(n);
//                if (index == -1) {
//                    dump(" 0");
//                } else {
//                    dump(" " + (index + 1));
//                }
//            }
//        }
//        dumpln(";");
//        dumpln("");
//
//        // now dump the VNF order parameter
//        dump("param order: " + vnfString + ":= ");
//        List<VNF> sfc;
//
//        for (Flow f : flows.values()) {
//            sfc = f.getSFC().getVnfList();
//
//            dumpln("");
//
//            dump(f.toString());
//
//            for (VNF v : VNF.getVnfList()) {
//                index = sfc.indexOf(v);
//
//                if (index == -1) {
//                    dump(" 0");
//                } else {
//                    dump(" " + (index + 1));
//                }
//            }
//        }
//
//        dumpln(";");
//        dumpln("");
//
//        // finally, dump the rate of all the flows
//        dumpln("param rate " + ":= ");
//        for (Flow f : flows.values()) {
//            dump(" " + f.toString() + " " + f.getBitRate());
//        }
//        dumpln(";");
//        dumpln("");
//        dumpln("option solver gurobi;"); // Use Gurobi solver;
//        dumpln("solve;"); // Instruct AMPL to solve this file
//        dumpln("");
    }

    protected Node createNodeWithId(int nodeId) {
        String nodeName = name + "-N" + nodeId;

        Node n = new Node(this, graph, nodeId, nodeName, Node.DEFAULT_CAPACITY);
        nodes.put(nodeId, n);
        graph.addVertex(n);

        return n;
    }

    protected Node createNodeWithId(int nodeId, int nodeType) {
        String nodeName = name + "-N" + nodeId;

        Node n = new Node(this, graph, nodeId, nodeName, Node.DEFAULT_CAPACITY, nodeType);
        nodes.put(nodeId, n);
        graph.addVertex(n);

        return n;
    }

    protected Node createNode() {
        int nodeId = nodes.size() + 1;

        return createNodeWithId(nodeId);
    }

    protected Link addEdge(Node n1, Node n2) {
        return (Link) graph.addEdge(n1, n2);
    }

    protected Flow createFlowBetweenNodes(Node src, Node dst, SFC sfc, int bitRate) {
        return createFlowBetweenNodes(src, dst, sfc, bitRate, null);
    }
    
    protected Flow createFlowBetweenNodes(Node src, Node dst, SFC sfc, int bitRate, GraphPath path) {

        GraphPath<Node, Link> gp;

        if (src.equals(dst)) {
            System.out.println("Cannot create flow when src,dst are equal");
            return null;
        }

        //    System.out.println("requesting path between " + src.getNodeName()
        //            + " and " + dst.getNodeName());
        if (path == null) {
            System.out.println("Null path. Computing path");
            gp = getPath(src, dst);
        } else {
            gp = path;
        }

        //    System.out.println("path found");
        //    System.out.println("Routing a flow along the path " + gp.toString());
        List<Link> pathLinks = gp.getEdgeList();
        List<Node> pathNodes = gp.getVertexList();

        int flowId = flows.size() + 1;
        //create the flow object
        Flow flow = new Flow(bitRate, flows.size() + 1, sfc, gp);

        flows.put(flowId, flow);

        // Now walk through the path and create the Flow Nodes.
        Iterator linkItr = pathLinks.iterator();
        Iterator nodeItr = pathNodes.iterator();

        Link prevl = null, nextl = null;
        Node n;
        Port ingressPort, egressPort;

        nextl = (Link) linkItr.next();

        info("Attempting initial placement for flow " + flow.toString());

        while (nodeItr.hasNext()) {

            n = (Node) nodeItr.next();

            if (prevl == null) { // this is the first node
                ingressPort = n.getHostPort();
            } else {
                ingressPort = prevl.getPortOnNode(n);
            }

            if (nextl == null) { // we have reached the last node
                egressPort = n.getHostPort();
            } else {
                egressPort = nextl.getPortOnNode(n);
            }

            n.addFlowNode(flow, ingressPort, egressPort);

//            info("createFlow: Processing " + prevl.toString() + " " + nextl.toString());
            prevl = nextl;

            if (linkItr.hasNext()) {
                nextl = (Link) linkItr.next();
            } else {
                nextl = null;
            }
        }

        placeVNFsForDance(flow, sfc, sfc.getFirstIndex(),
                gp.getVertexList(), 0 /* path index */, SFC.BEGIN_MARKER);

//        // Flow nodes have been created- now create the VNF Nodes
//        pathNodes = gp.getVertexList();
//        Iterator vnfItr = sfc.getVnfList().iterator();
//        VNF vnf;
//        Node lastNode = null;
//        FlowNode fnode;
//        int vnfIndex, prevIndex, nextIndex;
//
//        System.out.println("DANCE: Placing VNFs for flow " + flow.toString()
//                + " from " + src.toString() + " to " + dst.toString());
//
//        // initialize the index variables
//        prevIndex = SFC.BEGIN_MARKER;
//        nextIndex = sfc.getFirstIndex();
//
//        // place one VNF in each node until last node is reached-
//        // if there are still VNFs to place, place them at the last node
//        for (Node nd : pathNodes) {
//
//            // get the flow node of this flow on this node
//            fnode = nd.getFlowNodeByFlowId(flow.getFlowId());
//
//            // if Node does not have capacity and there are more VNFs to place,
//            // then, don't place any VNFs (skip the node)
//            if ((nd.getRemainingCapacity(Heuristic.DANCE) <= 0) && vnfItr.hasNext()) {
//                // TBD- What if this is the DST node?
//                // no remaining capacity- do not advance vnf index
//                fnode.setPreviousIndex(prevIndex);
//                fnode.setNextIndex(nextIndex);
//                System.out.println("Node " + nd.toString() + " has zero remaining capacity. "
//                        + "Skipping it for flow " + flow.toString());
//                lastNode = nd;
//                continue;
//            }
//
//            // Node has remaining capacity OR there are no more VNFs to place
//            if (vnfItr.hasNext()) {
//                vnf = (VNF) vnfItr.next();
//                // there are more VNFs to place
//
//            } else {
//                // SFC is exhausted (no more VNFs), but there are more nodes
//                prevIndex = sfc.getLastIndex();
//                nextIndex = SFC.END_MARKER;
//                fnode.setPreviousIndex(prevIndex);
//                fnode.setNextIndex(nextIndex);
//                lastNode = nd;
//                continue;
//            }
//
//            // get the flow node of this flow on this node
//            fnode = nd.getFlowNodeByFlowId(flow.getFlowId());
//
//            // find the index of the VNF in the SFC
//            vnfIndex = sfc.indexOf(vnf);
//
//            fnode.setPreviousIndex(sfc.getPreviousIndex(vnfIndex));
//            fnode.setNextIndex(sfc.getNextIndex(vnfIndex));
//
//            // place the VNF on this node for this flow
//            nd.addVnfNode(flow, vnf, Heuristic.DANCE);
//            lastNode = nd; // last node at which VNF was placed
//
//            // we just placed a VNF- update prev and next index
//            prevIndex = vnfIndex;
//            nextIndex = sfc.getNextIndex(vnfIndex);
//
//        }
//
//        while (vnfItr.hasNext()) {
//            // there are still VNFs left to be placed
//            vnf = (VNF) vnfItr.next();
//
//            if (lastNode.getRemainingCapacity(Heuristic.DANCE) <= 0) {
//                System.out.println("DANCE: Not enough capacity for  " + flow.toString());
//            }
//
//            lastNode.addVnfNode(flow, vnf, Heuristic.DANCE);
//        }
//
//        // all VNFs have been placed
//        // update the number of VNF instances in the network
//        // NOT USED
//        // set the last index of the Flow Node on the destination node
//        fnode = lastNode.getFlowNodeByFlowId(flow.getFlowId());
//        fnode.setNextIndex(SFC.END_MARKER);
        numVnfInstances += sfc.getVnfList().size();

        FlowNode fnode;
        // now populate the push / pull maps of the
        // ingress and egress ports of each flow node
        for (Node nd : pathNodes) {
            // get the flow node of this flow on this node
            fnode = nd.getFlowNodeByFlowId(flow.getFlowId());

            fnode.addFlowToPushPullMaps();
        }

        // Do VNF placement for NETPACK Heuristic
        placeVNFsForNetPack(flow, sfc, gp);

        pathNodes = gp.getVertexList();
        int sfcIndex = 0, pathIndex = 0;
        placeVNFsForHeuristicA(flow, sfc, sfcIndex, pathNodes, pathIndex);

        return flow;
    }

    private void placeVNFsForDance(Flow flow, SFC sfc, int sfcIndex,
            List<Node> pathNodes, int pathIndex, int lastIndex) {

        Node node;
        VNF vnf;
        FlowNode fnode;

        if (sfcIndex >= sfc.getLength()) {
            // all VNFs have been placed
            if (pathIndex >= pathNodes.size()) {
                //done
                info("DANCE: Done with flow " + flow.toString());
                return;
            } else {
                // there are one or more nodes along the path
                fnode = pathNodes.get(pathIndex).getFlowNodeByFlowId(flow.getFlowId());

                if (lastIndex != sfc.getLastIndex()) {
                    info("DANCE: WARNING- SFC exhausted by last index is incorrect");
                }
                fnode.setPreviousIndex(lastIndex);
                fnode.setNextIndex(sfc.getNextIndex(lastIndex));

                // go to the next node
                placeVNFsForDance(flow, sfc, sfcIndex, pathNodes, pathIndex + 1, lastIndex);
                return;
            }
        }

        // There are more VNFs to place
        // There are still more VNFs to place
        if (pathIndex >= pathNodes.size()) {
            // This should NOT happen since we're packing remaining VNFs on last node
            info("DANCE: Unable to place all VNFS for " + flow.toString());
        }

        info("DANCE: Placing SFC " + sfcIndex + " for flow " + flow.toString()
                + " at node " + pathNodes.get(pathIndex).toString());

        // attempt to place VNF on node
        node = pathNodes.get(pathIndex);

        if ((pathIndex + 1) >= pathNodes.size()) {
            info("DANCE: Packing all VNFs on final node for " + flow.toString());

            if (node.getRemainingCapacity(Heuristic.DANCE) <= 0) {
                info("DANCE: WARNING: capacity exceeded on last node");
            }
            placeVNFonNodeForDance(node, flow, sfc, sfcIndex, lastIndex);

            // stay on the last node, but proceed with next VNF
            if (sfcIndex + 1 < sfc.getLength()) {
                placeVNFsForDance(flow, sfc, sfcIndex + 1, pathNodes, pathIndex, lastIndex);
            } else {
                // done
                info("DANCE: Done packing VNFs on last node");
                return;
            }
        } else if (node.getRemainingCapacity(Heuristic.DANCE) > 0) {
            // there is remaining capacity
            // place the current VNF on this node
            // proceed with next VNF on next node
            placeVNFonNodeForDance(node, flow, sfc, sfcIndex, lastIndex);

            // sfcIndex is now the lastIndex (index of last placed VNF)
            placeVNFsForDance(flow, sfc, sfcIndex + 1, pathNodes, pathIndex + 1, sfcIndex);

        } else {
            // This is NOT the last node, but it is out of capacity.
            // proceed to next node
            info("DANCE: node " + node.toString() + " out of capacity. "
                    + " Proceeding to next node for flow " + flow.toString()
                    + " previndex = " + lastIndex + " nextIndex = " + sfcIndex);

            // there are one or more nodes along the path
            fnode = pathNodes.get(pathIndex).getFlowNodeByFlowId(flow.getFlowId());

            // set prev, next index for the node before proceeding
            fnode.setPreviousIndex(lastIndex); // last placed VNF
            fnode.setNextIndex(sfcIndex); // to be placed next 

            placeVNFsForDance(flow, sfc, sfcIndex, pathNodes, pathIndex + 1, lastIndex);
        }

        return;
    }

    private void placeVNFonNodeForDance(Node node, Flow flow, SFC sfc, int sfcIndex,
            int lastIndex) {
        FlowNode fnode;
        VNF vnf;

        int nextIndex = sfc.getNextIndex(sfcIndex);
        info("DANCE: Placing SFC " + sfcIndex + " on " + node.toString()
                + " prevIndex = " + lastIndex + " nextIndex = " + nextIndex);

        fnode = node.getFlowNodeByFlowId(flow.getFlowId());
        vnf = sfc.getVnfByIndex(sfcIndex);

        fnode.setPreviousIndex(lastIndex);

        fnode.setNextIndex(nextIndex);

        // place the VNF on this node for this flow
        node.addVnfNode(flow, vnf, Heuristic.DANCE);
        return;
    }

    private void placeVNFsForNetPack(Flow flow, SFC sfc, GraphPath path) {

        List<Node> pathNodes;

        info("Placing VNFs using NetPack heuristic for " + flow.toString());

        // Flow nodes have been created- now create the VNF Nodes
        pathNodes = path.getVertexList();

        int vnfIndex = 0;
        VNF vnf;

        for (Node nd : pathNodes) {
            // place as many VNFs as possible on each node until 
            // all VNFs are placed
            while (nd.getRemainingCapacity(Heuristic.NETPACK) > 0) {

                if (vnfIndex >= sfc.getLength()) {
                    break;
                }

                info("NetPack: Placing VNFs for " + flow.toString() + " at " + nd.toString()
                        + ". Remaining capacity = " + nd.getRemainingCapacity(Heuristic.NETPACK));

                vnf = sfc.getVnfByIndex(vnfIndex);

                nd.addVnfNode(flow, vnf, Heuristic.NETPACK);

                vnfIndex++;
            }

            if (nd.getRemainingCapacity(Heuristic.NETPACK) <= 0) {
                info("Ran out of capacity at " + nd.toString()
                        + " while placing VNFs for " + flow.toString());
            }

            if (vnfIndex >= sfc.getLength()) {
                info("Finished placing all VNFs for " + flow.toString());
                break;
            }
        }

        if (vnfIndex < sfc.getLength()) {
            System.out.println("NetPack: Unable to place all VNFs for flow " + flow.toString());
        }
    }

    private class VNFPlacement {

        private VNF vnf;
        private int pathIndex;

        public VNFPlacement(VNF vnf) {
            this.vnf = vnf;
        }

        public VNF getVnf() {
            return vnf;
        }

        public int getPathIndex() {
            return pathIndex;
        }

        public void setPathIndex(int pathIndex) {
            this.pathIndex = pathIndex;
        }

    }

    private void placeVNFsForHeuristicA(Flow flow, SFC sfc, int sfcIndex,
            List<Node> pathNodes, int pathIndex) {

        Node nd;
        VNF vnf;
        VNFPlacement placement;

        if (sfcIndex >= sfc.getLength()) {
            // all VNFs have been placed
            return;
        }

        if (pathIndex >= pathNodes.size()) {
            info("Heuristic-A: Unable to place all VNFS for " + flow.toString());
            return;
        }

        info("Heuristic-A: Placing SFC " + sfcIndex + " for flow " + flow.toString()
                + " at node " + pathNodes.get(pathIndex).toString());

        // try to find a heuristic placement for the current VNF
        vnf = sfc.getVnfByIndex(sfcIndex);
        placement = new VNFPlacement(vnf);
        nd = pathNodes.get(pathIndex);

        if (checkVNFPlacementForHeuristicA(flow, sfc, sfcIndex,
                pathNodes, pathIndex, placement) == false) {
            // heuristic placement is not possible. Place on current node
            // but first check if there is capacity
            info("Heuristic placement failed");

            if (nd.getRemainingCapacity(Heuristic.HEURISTIC_A) <= 0) {
                info("Heuristic-A: Capacity reached at " + nd.toString() + " for " + flow.toString());

                // proceed to the next node
                placeVNFsForHeuristicA(flow, sfc, sfcIndex, pathNodes, pathIndex + 1);

            } else {
                info("Final placement of VNF for " + flow.toString() + " at " + nd.toString());

                // there is capacity on the current node
                nd.addVnfNode(flow, vnf, Heuristic.HEURISTIC_A);

                // stay on the current node
                placeVNFsForHeuristicA(flow, sfc, sfcIndex + 1, pathNodes, pathIndex);

            }

        } else {

            info("Heuristic placement SUCCEEDED");
            // a feasible heuristic placement 
            nd = pathNodes.get(placement.getPathIndex());

            nd.addVnfNode(flow, vnf, Heuristic.HEURISTIC_A);

            placeVNFsForHeuristicA(flow, sfc, sfcIndex + 1, pathNodes, placement.getPathIndex());
        }
    }

    private boolean checkVNFPlacementForHeuristicA(Flow flow, SFC sfc, int sfcIndex,
            List<Node> pathNodes, int pathIndex, VNFPlacement placement) {

        if (pathIndex >= pathNodes.size()) {
            return false;
        }

        if (sfcIndex >= sfc.getLength()) {
            // all VNFs in SFC have been placed
            return true;
        }

        VNF vnf = sfc.getVnfByIndex(sfcIndex);

        Node nd = pathNodes.get(pathIndex);
        info("Checking Heuristic placement for " + flow.toString() + " on " + nd.toString());

        if (nd.getVNFUtility(vnf, Heuristic.HEURISTIC_A) > 0
                && nd.getRemainingCapacity(Heuristic.HEURISTIC_A) > 0) {
            // check if other nodes can be placed
            nd.addVnfNode(flow, vnf, Heuristic.HEURISTIC_A);

            // place the rest of the VNFs
            if (checkVNFPlacementForHeuristicA(flow, sfc, sfcIndex + 1, pathNodes, pathIndex, placement) == true) {
                placement.setPathIndex(pathIndex);

                info(" Able to place VNF for " + flow.toString() + " at node " + nd.toString());
                // release the resources
                nd.removeVnfNode(flow, vnf, Heuristic.HEURISTIC_A);

                return true;
            } else {

                info("Heuristic placement failed- rolling back");
                nd.removeVnfNode(flow, vnf, Heuristic.HEURISTIC_A);

                return false;
            }
        }

        info("Proceeding to next node");

        // else, it is not possible to place VNF in current node
        // proceed to the next node and check possibility of placement
        return checkVNFPlacementForHeuristicA(flow, sfc, sfcIndex, pathNodes, pathIndex + 1, placement);

    }

    @Override
    public void body() {

        Network next = null;
        double round;

        System.out.println("Network " + name + " starting");

        for (Node node : nodes.values()) {
            // schedule the first set of events for all the nodes 
            // for the start round

            sim_schedule(node.get_id(), startRound, Message.Type.TRIGGER);
        }

//        for (int i = 0; i < (maxRounds - 2); i++) {
        while (Sim_system.running()) {

            round = Sim_system.clock();

            if (round == endRound + 1) {
                System.out.println("++++++++++++++++++++++++++++++++++");
                System.out.println("FINAL output for network " + name);

                calculateLinkUsage();
                prettyPrint();
                System.out.println("++++++++++++++++++++++++++++++++++");
            }

            if (round == startRound) {
                prettyPrint((int) round);
            }

            if (round <= startRound) {
                sim_pause(1.0);
                continue;
            }

            if (round > endRound) {
                break;
            }

            if ((round % ADDNL_FLOWS_INTERVAL == 0)
                    && ((endRound - round) > ADDNL_FLOWS_INTERVAL) && (IS_DYNAMIC == 1)) {
                // every tenth round, add 10 more flows
                networkTest.addAdditionalFlows(20);
            }

            if ((round % OPT_REPORT_INTERVAL == 0) && (IS_DYNAMIC == 1)) {
                prettyPrint((int) round);
            }

            for (Node node : nodes.values()) {
                // for each node, raise an event to do one
                // round of processing

                sim_schedule(node.get_id(), 1.0, Message.Type.TRIGGER); // NEW
            }

            // pause for 1 round and raise the next set of events.
            sim_pause(1.0);
        }

        //    networkTest.unitTestCheck();
        networkTest.sanityCheckVnfInstances();

        dumpInfo();
        writer.close();
        xyWriter.close();

//        if (next != null) {
//            next.sim_schedule(next.get_id(), 0.0, Message.Type.TRIGGER);
//        }
    }

    private GraphPath getPath(Node src, Node dst) {

        List<GraphPath<Node, Link>> kshortestpaths;
        GraphPath<Node, Link> shortestPath, nextPath;
        int shortestDistance, numShortestPaths, randomIndex;

        if (src.equals(dst)) {
            return null;
        }

        if (!ecmp) {
            System.out.println("Finding path using DSP");
            return dsp.getPath(src, dst);
        }

        // KSHORTEST PATH is ACTIVE
        kshortestpaths = ksp.getPaths(src, dst);

        shortestPath = kshortestpaths.get(0);
        shortestDistance = shortestPath.getLength();
        numShortestPaths = 1;

        for (int i = 1; i < kshortestpaths.size(); i++) {
            nextPath = kshortestpaths.get(i);
            if (nextPath.getLength() > shortestDistance) {
                break;
            }
            numShortestPaths += 1;
        }

        if (numShortestPaths == 1) {
            return kshortestpaths.get(0);
        }

        // there are multiple shortest paths- randomly pick one
        randomIndex = (int) (Math.random() * numShortestPaths);

        return kshortestpaths.get(randomIndex);
    }

    protected Node getNodeByNodeId(int nodeId) {
        return (Node) nodes.get(nodeId);
    }

    protected Node getRandomNode() {
        return getRandomNode(null);
    }

    protected Node getRandomNode(Node excludedNode) {
        Object[] allNodes;
        Node randomNode;
        int numNodes = nodes.size();
        int index;

        allNodes = (nodes.values().toArray());

        index = (int) (Math.random() * numNodes);
        randomNode = (Node) allNodes[index];

        while (randomNode.equals(excludedNode)) {
            // try again
            index = (int) (Math.random() * numNodes);
            randomNode = (Node) allNodes[index];
        }

        return randomNode;
    }

    protected Flow getFlowByFlowId(int flowId) {
        return (Flow) flows.get(flowId);
    }

    public Logger getLogger() {
        return logger;
    }

    public static void info(String msg) {
        if (logger.isLoggable(Level.INFO)) {
            System.out.println(msg);
        }
    }

    public static void warning(String msg) {
        if (logger.isLoggable(Level.WARNING)) {
            System.out.println("WARNING: " + msg);
        }
    }

    public static double clock() {
        return Sim_system.clock();
    }

    private void calculateLinkUsage() {

        int activeLinks = 0;
        this.maxLinkUsage = 0;
        int linkUsage;
        double bwUsage;
        double totalBwUsage = 0;

        for (Link l : graph.edgeSet()) {
            linkUsage = l.getUsage();
            bwUsage = l.getBwUsage((int) getRoundsTaken());

            totalLinkUsage += linkUsage; // in bytes
            totalBwUsage += bwUsage;

            if (this.maxLinkUsage < linkUsage) {
                this.maxLinkUsage = linkUsage;
            }

            if (this.maxBwUsage < bwUsage) {
                this.maxBwUsage = bwUsage;
            }

            totalMsgCount += l.getMsgCount();
            if (linkUsage == 0) {
                continue;
            }

            activeLinks++;
        }

        avgLinkUsage = totalLinkUsage / activeLinks;
        avgMsgCount = totalMsgCount / activeLinks;
        avgBwUsage = totalBwUsage / activeLinks;

    }

    private void updateCurrentVnfInstanceCount() {
        vnfInstances = 0;
        vnfFlowCount = 0;

        for (Node n : nodes.values()) {
            //    n.prettyPrint();
//            finalLoad += n.getLoad();
            vnfInstances += n.getVnfInstanceCount(Heuristic.DANCE);
            vnfFlowCount += n.getVnfFlowCount(Heuristic.DANCE);

        }
    }

    public void prettyPrint() {

        updateCurrentVnfInstanceCount();
//
        System.out.println("Topo " + topo + "(" + ftTopoK + "," + ftTopoH + "); "
                + "SFC Len = " + sfcLen
                + "; Rounds taken " + getRoundsTaken()
                + "; Number of flows = " + flows.size());

        System.out.println("Num of VNF instances = " + initialVnfInstances + " (Initial) "
                + vnfInstances + " (Final) " + netPackVnfInstances
                + " (NetPack) " + heuristicAVnfInstances + " (Heuristic A) ");

        System.out.println("Num of VNF flow instances = " + initialVnfFlowCount + " (Initial) "
                + vnfFlowCount + " (Final)");

        System.out.println("# Total Link Usage = " + totalLinkUsage + "; Max Link Usage " + maxLinkUsage
                + "; Avg Link Usage = " + avgLinkUsage);

        System.out.println("# Total Msg Count = " + totalMsgCount + "; Avg Msg Count = " + avgMsgCount);

        System.out.println("# Max Bw Usage = " + maxBwUsage + " PPM; Avg Bw Usage = " + avgBwUsage + " PPM");
       
        System.out.println("# Estimated Time Taken = " + getRoundsTaken() * MSECS_PER_ROUND + " msecs");
    }

    // print an interim report at a given round for the dynamic case
    public void prettyPrint(int round) {

        updateCurrentVnfInstanceCount();

        System.out.println(round + "," + flows.size() + "," + vnfInstances);

//        System.out.println("At Round " + round +  " - Number of flows = " + flows.size() + "; Num of VNF instances = " + vnfInstances + " (Current)" +
//                "; Num of VNF flow instances = " + vnfFlowCount + " (Current)");
    }

    // Methods that return the STATS required for the graph plot
    protected int roundsTaken() {
        return (int) numRounds - (int) startRound;
    }

    protected int initialVnfInstances() {
        return initialVnfInstances;
    }

    protected int finalVnfInstances() {
        return vnfInstances;
    }
}
