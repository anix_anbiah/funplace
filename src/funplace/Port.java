/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import eduni.simjava.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anix- Port class captures a port on a node
 */
public class Port {

    public static int HOST_PORT_ID = 1000; // reserve one port as source/sink for
    // all flows originating/terminating at this node

    public static int PORT_TYPE_HOST = 1;
    public static int PORT_TYPE_NET = 2;

    private final static Logger logger = Logger.getLogger(Port.class.getName());

    int portId; //port ID
    Node node; // local node
    Node neighbor; // neighbor connected to this port
    String linkName;
    Link link;
    int type;
    boolean debug;

    String portName;
    Sim_port simPort;

    List<Flow> ingressFlows;
    List<Flow> egressFlows;

    List<Message> trQueue; // queue of outgoing transfer requests

    // ingress and egress flow maps are indexed by the next adjacent
    // function in the SFC, i.e. the next function that can be pulled 
    // from downstream or upstream nodes
    Map<VNF, List<FlowNode>> ingressPullMap;
    Map<VNF, List<FlowNode>> egressPullMap;

    Map<VNF, List<FlowNode>> ingressPushMap;
    Map<VNF, List<FlowNode>> egressPushMap;

    // Constructor for a NET type port
    public Port(Node node, Node neighbor, int portId, Link link) {

        this(node, portId);

        this.neighbor = neighbor;
        this.link = link;
        this.type = PORT_TYPE_NET;

        linkName = link.getName();
        portName = node.getNodeName() + "-" + linkName;

//        System.out.println("Adding port with name " + portName + " at node " + node.getNodeName());
        // naming convention of the port is "<nodeName>-<linkName>"
        simPort = new Sim_port(portName);

        node.add_port(simPort);

        if (node.get_port(portName) == null) {
            System.out.println("Port " + portName + " not found on node " + node.getNodeName());
        }

    }

    // constructor for a HOST type port
    public Port(Node node, int portId) {
        this.node = node;
        this.portId = portId;
        this.neighbor = null;
        this.link = null;
        this.type = PORT_TYPE_HOST;

        logger.setLevel(Network.defaultLogLevel);
        this.debug = false;

        this.trQueue = new ArrayList<>();

        this.ingressFlows = new ArrayList();
        this.egressFlows = new ArrayList();

        this.ingressPullMap = new HashMap<>();
        this.ingressPushMap = new HashMap<>();
        this.egressPullMap = new HashMap<>();
        this.egressPushMap = new HashMap<>();

        linkName = "HOST";
        portName = node.getNodeName() + "-" + linkName;

    }

    public String getPortName() {
        return portName;
    }

    public Link getLink() {
        return link;
    }

    public int getPortId() {
        return portId;
    }

    public Sim_port getSimPort() {
        return simPort;
    }

    public void addFlow(Flow flow, boolean ingress) {

        if (ingress) {
            ingressFlows.add(flow);
        } else {
            egressFlows.add(flow);
        }
    }

    private void addFlowToPushPullMap(FlowNode fnode, VNF vnf,
            boolean ingress, boolean pull) {

        List<FlowNode> fNodeList;
        Map<VNF, List<FlowNode>> map;

        if (ingress) {
            //ingress
            if (pull) {
                map = ingressPullMap;
            } else {
                map = ingressPushMap;
            }
        } else {
            //egress
            if (pull) {
                map = egressPullMap;
            } else {
                map = egressPushMap;
            }
        }

        fNodeList = map.get(vnf);
        if (fNodeList == null) {
            fNodeList = new ArrayList<>();
            map.put(vnf, fNodeList);
        }

        fNodeList.add(fnode);
    }

    public void removeFlowFromPushPullMap(FlowNode fnode, VNF vnf,
            boolean ingress, boolean pull) {

        List<FlowNode> fNodeList;
        Map<VNF, List<FlowNode>> map;

        if (ingress) {
            //ingress
            if (pull) {
                map = ingressPullMap;
            } else {
                map = ingressPushMap;
            }
        } else {
            //egress
            if (pull) {
                map = egressPullMap;
            } else {
                map = egressPushMap;
            }
        }

        fNodeList = map.get(vnf);

        if (fNodeList == null) {
            warning("Empty FNODE list for VNF" + vnf.toString());
            return;
        }

        fNodeList.remove(fnode);

    }

    public void addFlowIngressPullMap(FlowNode fnode, VNF vnf) {

        if (type == PORT_TYPE_HOST) {
            return;
        }
        addFlowToPushPullMap(fnode, vnf, true, true);
    }

    public void removeFlowIngressPullMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        removeFlowFromPushPullMap(fnode, vnf, true, true);
    }

    public void addFlowEgressPullMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        addFlowToPushPullMap(fnode, vnf, false, true);
    }

    public void removeFlowEgressPullMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        removeFlowFromPushPullMap(fnode, vnf, false, true);
    }

    public void addFlowIngressPushMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        addFlowToPushPullMap(fnode, vnf, true, false);
    }

    public void removeFlowIngressPushMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        removeFlowFromPushPullMap(fnode, vnf, true, false);
    }

    public void addFlowEgressPushMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        addFlowToPushPullMap(fnode, vnf, false, false);
    }

    public void removeFlowEgressPushMap(FlowNode fnode, VNF vnf) {
        if (type == PORT_TYPE_HOST) {
            return;
        }
        removeFlowFromPushPullMap(fnode, vnf, false, false);
    }

    // find the localDifferential for the given VNF  
    // for this port. Differential is the potential increase in utility
    // for this VNF by pulling the VNF from either egress or ingress side
    public int findDifferential(VNF vnf) {

        List<FlowNode> ingressVnfFlows;
        List<FlowNode> egressVnfFlows;

//        Network.warning("findDifferential at port " + toString() + 
//                " for VNF " + vnf.toString());
        // TBD replace list of flows with FlowGroup
        ingressVnfFlows = ingressPullMap.get(vnf);
        egressVnfFlows = egressPullMap.get(vnf);

        trQueue = new ArrayList<>();

        int differential = 0;

        if (egressVnfFlows != null) {
            for (FlowNode fNode : egressVnfFlows) {
                differential += fNode.getFlow().getBitRate();
            }
        } else {
//            Network.warning("Differential at port " + toString() +
//                    "for VNF " + vnf.toString() + " No egress flows");
        }

        if (ingressVnfFlows != null) {
            for (FlowNode fNode : ingressVnfFlows) {
                differential += fNode.getFlow().getBitRate();
            }
        } else {
//           Network.warning("Differential at port " + toString() +
//                    "for VNF " + vnf.toString() + " No ingress flows");
        }

//        Network.warning("Differential = " + localDifferential);
        return differential;

    }

    // transferForce- transfers differential to all flows on this port
    // in both directions
    // TBD- THIS FUNCTION IS NOT USED ANY MORE
//    public void transferForce(VNF vnf, int differential, int maxUtility) {
//
//        Network.warning("Transfering forces for " + vnf.toString()
//                + " at port " + toString() + "; Differential = " + differential
//                + "; Max Utility = " + maxUtility);
//
//        List<FlowNode> ingressVnfFlows, egressVnfFlows;
//        // get all flows in ingress followed by egress push map
//        if (differential > 0) {
//            //pull request- TBD explain why push map is used here
//            ingressVnfFlows = ingressPushMap.get(vnf);
//            egressVnfFlows = egressPushMap.get(vnf);
//
//        } else {
//            //push request
//            ingressVnfFlows = ingressPullMap.get(vnf);
//            egressVnfFlows = egressPullMap.get(vnf);
//        }
//
//        if (ingressVnfFlows != null) {
//            transferForceToFlows(ingressVnfFlows, true, differential, maxUtility, neighbor.getNodeId());
//        }
//
//        if (egressVnfFlows != null) {
//            transferForceToFlows(egressVnfFlows, false, differential, maxUtility, neighbor.getNodeId());
//        }
//    }
    // TBD- THIS FUNCTION IS NOT USED ANY MORE
//    private void transferForceToFlows(List<FlowNode> flows, boolean isUpstream, int differential,
//            int maxUtility, int srcNodeId) {
//
//        Network.warning("Transferring force");
//
//        for (FlowNode fn : flows) {
//
//            if (isUpstream) {
//                fn.usDifferential = differential;
//                fn.usMaxUtility = maxUtility;
//                fn.usDiffNodeId = srcNodeId;
//            } else {
//                fn.dsDifferential = differential;
//                fn.dsMaxUtility = maxUtility;
//                fn.dsDiffNodeId = srcNodeId;
//            }
//        }
//
//    }
//    OLD - public void transferForce(int vnfId, int localDifferential) {
//        
//        Map<Port, FlowGroup> targetPorts = new HashMap<>();
//        FlowGroup fg;
//        
//        // get all flows in ingress followed by egress pull map
//        List<FlowNode> ingressFlows = ingressPushMap.get(vnfId);
//
//
//        for (FlowNode fn : ingressFlows) {
//
//            fn.upstreamDiffReceived = localDifferential; // TBD- is this required?
//
//            if (!targetPorts.containsKey(fn.egressPort)) {
//                targetPorts.put(fn.egressPort, new FlowGroup());
//            }
//
//            fg = targetPorts.get(fn.egressPort);
//            fg.setDifferential(localDifferential);
//            fg.addFlow(fn.getFlow());
//
//        }
//
//        for (Port tp : targetPorts.keySet()) {
//            tp.enqueueTransferRequest(targetPorts.get(tp));
//        }
//
//        // TBD- repeat the same thing for flows in the other direction
//        List<FlowNode> egressFlows = egressPushMap.get(vnfId);
//
//        for (FlowNode fn : egressFlows) {
//            fn.downstreamDiffReceived = localDifferential;
//        }
//
//    }
    public void resetTRQueue() {
        trQueue.clear();
    }

    public void transferForce(Message msg) {

        int differential = msg.differential;
        int maxUtility = msg.maxUtility;
        FlowGroup fgroup = msg.getFlowGroup();
        int srcNodeId = msg.srcNodeId;

        info("transferForce- Msg = " + msg.toString()
                + " at " + toString() + "; Number of flows is " + fgroup.getFlows().size());

        for (Flow flow : fgroup.getFlows()) {

            FlowNode fnode = node.getFlowNodeByFlowId(flow.getFlowId());

            if (ingressFlows.contains(flow)) {
                fnode.usVnf = msg.vnf;
                fnode.usDifferential = differential;
                fnode.usMaxUtility = maxUtility;
                fnode.usDiffNodeId = srcNodeId;

            } else if (egressFlows.contains(flow)) {
                fnode.dsVnf = msg.vnf;
                fnode.dsDifferential = differential;
                fnode.dsMaxUtility = maxUtility;
                fnode.dsDiffNodeId = srcNodeId;
            } else {
                Network.warning("Transfer Force- Neither ingress nor egress flow");
            }

        }
    }

    public void generateLocalTransferRequest(VNF vnf, int maxUtility, int differential) {
        Message tr;
        List<FlowNode> inflows;
        List<FlowNode> outflows;

        tr = new Message(Message.Type.REQUEST, vnf);
        tr.srcNodeId = node.getNodeId();
        tr.dstNodeId = 0; // address it to ALL nodes
        tr.maxUtility = maxUtility;
        tr.differential = differential;
        tr.link = getLink();

        inflows = ingressPullMap.get(vnf);
        outflows = egressPullMap.get(vnf);

        // since this is locally originated, TR applies to ALL flows
        // on this port, both ingress and egress. So, add them all to the TR  
        if (inflows != null) {
            for (FlowNode fnode : inflows) {
                tr.flowGroup.addFlow(fnode.getFlow());
            }
        }

        if (outflows != null) {
            for (FlowNode fnode : outflows) {
                tr.flowGroup.addFlow(fnode.getFlow());
            }
        }

        if (tr.flowGroup.getFlows().isEmpty()) {
            // no suitable flows
            return;
        }

        // SADHANA
        // node.sim_schedule(getSimPort(), 1.0, Message.Type.REQUEST, tr);
        transmitMsg(Message.Type.REQUEST, tr, 1.0);

        info("generated LOCAL transfer request at " + node.toString()
                + " for VNF " + vnf.toString());

    }

    public void generateTransferRequests(VNFNode vnfNode) {

        VNF vnf = vnfNode.getVnf();

//        info("generate transfer requests at Port " + toString()
//                + " for VNF " + vnfNode.toString());
        int localDifferential = vnfNode.getDifferential();
        int remoteDifferential, minRemoteDifferential = 9999999;

        // First create a map indexed by the source node (of the TR)
        // organize the flow groups in this map. 
        // 
        Map<Integer, FlowGroup> trMap = new HashMap<>();
        List<FlowNode> ingressFlowNodes = ingressPullMap.get(vnf);
        List<FlowNode> egressFlowNodes = egressPullMap.get(vnf);

        minRemoteDifferential = aggregateTransferRequests(trMap, vnf, ingressFlowNodes, true);
        remoteDifferential = aggregateTransferRequests(trMap, vnf, egressFlowNodes, false);
        if (remoteDifferential < minRemoteDifferential) {
            minRemoteDifferential = remoteDifferential;
        }

        // check if the local VNF Node has a lower differential
        // than minimum remote differential.
        // In this case, we must generate a locally originated TR
        if ((vnfNode.maxUtility > 0) && (localDifferential < minRemoteDifferential)) {
            // there is a locally hosted VNF instance

            info("Local differential is " + localDifferential);

            if (localDifferential > 0) {
                // positive localDifferential
                generateLocalTransferRequest(vnfNode.getVnf(), vnfNode.maxUtility, localDifferential);
            }

            return;
        }

        if (minRemoteDifferential == 0) {
            info("minRemoteDifferential is zero");
            // there is no remote differential
            return;
        }

        info("Remote DIFF non-zero for " + vnfNode.toString());

        FlowGroup fg;

        // else, remote differential is non-zero. In this case, 
        // transfer any forces to the neighbor (handle both directions)
        // Now, each flow group in the map becomes an outgoing TR
        for (int srcNodeId : trMap.keySet()) {
            Message trMsg = new Message(Message.Type.REQUEST, vnfNode.getVnf());
            fg = trMap.get(srcNodeId);

            trMsg.getFlowGroup().addFlows(fg.getFlows());
            trMsg.srcNodeId = srcNodeId;
            trMsg.dstNodeId = 0; // address it to ALL nodes
            trMsg.maxUtility = fg.maxUtility;
            trMsg.differential = fg.differential;
            trMsg.link = getLink();

            if (trMsg.getFlowGroup().getFlows().isEmpty()) {
                info("NO FLOWS in FORWARDED TR " + trMsg.toString()
                        + " AT NODE " + node.toString());
                continue;
            }

            if (trMsg.maxUtility == 0) {
                info("Max Utility is zeo in FORWARDED TR");
                continue;
            }

            // now schedule the Transfer Request on the port
            // SADHANA 
            transmitMsg(Message.Type.REQUEST, trMsg, 1.0);
            //node.sim_schedule(getSimPort(), 1.0, Message.Type.REQUEST, trMsg);

            info("generated FORWARDED transfer request at " + node.toString()
                    + " for VNF " + vnfNode.toString() + " to "
                    + neighbor.toString());
        }
    }

    private void transmitMsg(int type, Message msg, double delay) {

        link.updateUsage(msg.sizeOf());

        node.sim_schedule(getSimPort(), delay, type, msg);
    }

    public boolean pullVnf(VNFNode vNode) {

        // get the flows to be de-linked from the pull map
        // create a copy of the list to avoid co-modification exception
        // since we will be deleting items from the list
        List<FlowNode> inMapFlows = ingressPullMap.get(vNode.getVnf());
        List<FlowNode> inflows = new ArrayList<>();
        if (inMapFlows != null) {
            inflows.addAll(inMapFlows);
        }

        List<FlowNode> outMapFlows = egressPullMap.get(vNode.getVnf());
        List<FlowNode> outflows = new ArrayList<>();
        if (outMapFlows != null) {
            outflows.addAll(outMapFlows);
        }

        info("PULL for vnode " + vNode.toString()
                + " at port " + toString() + "; Inflows = " + inflows.size()
                + "; Outflows = " + outflows.size());

        // delink the inflows and outflows from the local VNF node
        if (pullFlowVnfs(inflows, vNode, true)) {
            // offers crossed on wire
            return true;
        }

        return pullFlowVnfs(outflows, vNode, false);
    }

    public void pushVnf(VNFNode vNode) {

        // get the flows to be de-linked from the push map
        // create a copy of the list to avoid co-modification exception
        // since we will be deleting items from the list
        List<FlowNode> inMapFlows = ingressPushMap.get(vNode.getVnf());
        List<FlowNode> inflows = new ArrayList<>();
        if (inMapFlows != null) {
            inflows.addAll(inMapFlows);
        }

        List<FlowNode> outMapFlows = egressPushMap.get(vNode.getVnf());
        List<FlowNode> outflows = new ArrayList<>();
        if (outMapFlows != null) {
            outflows.addAll(outMapFlows);
        }

        info("PUSH for vnode " + vNode.toString()
                + " at port " + toString() + "; Inflows = " + inflows.size()
                + "; Outflows = " + outflows.size());

        // delink the inflows and outflows from the local VNF node
        pushFlowVnfs(inflows, vNode, true);
        pushFlowVnfs(outflows, vNode, false);
    }

    private void pushFlowVnfs(List<FlowNode> fnodes, VNFNode vNode, boolean ingress) {

        if (fnodes == null) {
            // there are no flow nodes to process in this direction
            return;
        }

        for (FlowNode fnode : fnodes) {
            vNode.removeFlow(fnode.getFlow());

            // Now remove the flow from the push(pull) map and
            // insert into the pull(push) map
            fnode.pushVnf(vNode.getVnf(), ingress);
        }
    }

    // returns TRUE if offers crossed on the wire
    private boolean pullFlowVnfs(List<FlowNode> fnodes, VNFNode vNode, boolean ingress) {

        if (fnodes == null) {
            // there are no flow nodes to process in this direction
            return false;
        }

        for (FlowNode fnode : fnodes) {

            // Note- 'u/s offer pending' means that the offer is pending FROM
            // an upstream node. 'd/s offer pending' has corresponding meaning.
            // So, if we are pulling from the ingress (from an u/s node), we
            // have to check if d/s offer is pending and vice versa
            if (ingress && fnode.dsOfferPending) {
                info("pullFlowVnf- d/s offer pending fnode " + fnode.toString());
                if (fnode.usDiffNodeId < node.getNodeId()) {
                    // clear the ds differential if it is from
                    // a node will smaller ID, allowing the transfer
                    // if it is re-transmitted by the requesting node
                    fnode.usOfferPending = false;
                    fnode.usDiffNodeId = 0;
                    fnode.usVnf = null;
                    fnode.usOfferTimestamp = 0.0;
                }
                return true;
            }

            if (!ingress && fnode.usOfferPending) {
                info("pullFlowVnf- u/s offer pending fnode " + fnode.toString());
                if (fnode.dsDiffNodeId < node.getNodeId()) {
                    // clear the ds differential if it is from
                    // a node will smaller ID, allowing the transfer
                    // if it is re-transmitted by the requesting node
                    fnode.dsOfferPending = false;
                    fnode.dsDiffNodeId = 0;
                    fnode.dsVnf = null;
                    fnode.dsOfferTimestamp = 0.0;
                }
                return true;
            }

            vNode.addFlow(fnode.getFlow());

            // Now remove the flow from the push(pull) map and
            // insert into the pull(push) map
            fnode.pullVnf(vNode.getVnf(), ingress);
        }

        return false;
    }

    protected boolean sanityCheckTransferRequest(Message trMsg) {

        if (trMsg.getFlowGroup().getFlows().isEmpty()) {
            return false;
        }

        boolean flowFound = false;
        for (Flow sampleFlow : trMsg.getFlowGroup().getFlows()) {

            flowFound = false;
            FlowNode sampleFlowNode = node.getFlowNodeByFlowId(sampleFlow.getFlowId());

            List<FlowNode> inFlows = ingressPushMap.get(trMsg.vnf);
            List<FlowNode> outFlows = egressPushMap.get(trMsg.vnf);

            if (inFlows != null) {
                if (inFlows.contains(sampleFlowNode)) {
                    flowFound = true;
                }
            }

            if (outFlows != null) {
                if (outFlows.contains(sampleFlowNode)) {
                    flowFound = true;
                }
            }

            if (!flowFound) {
                // looks like we have already "moved the pointer" for this flow
                // the requested VNF is no longer available for transfer            
                return false;
            }

        }
        return true;
    }

//    private void pushFlowVnf(FlowNode fnode, VNFNode vnode, boolean ingress) {
//
////        List<FlowNode> fNodes;
////
////        // delete the flow from the correct push map
////        if (ingress) {
////            fNodes = ingressPushMap.get(vnode.getVnf());
////        } else {
////            fNodes = egressPushMap.get(vnode.getVnf());
////        }
////
////        fNodes.remove(fnode);
//
//        fnode.pushVnf(vnode.getVnf(), ingress);
//
//    }
//
//    private void pullFlowVnf(FlowNode fnode, VNFNode vnode, boolean ingress) {
//
////        List<FlowNode> fNodes;
//
////        // delete the flow from the correct pull map
////        if (ingress) {
////            fNodes = ingressPullMap.get(vnode.getVnf());
////        } else {
////            fNodes = egressPullMap.get(vnode.getVnf());
////        }
////
////        fNodes.remove(fnode);
//
//        fnode.pullVnf(vnode.getVnf(), ingress);
//
//    }
    public void generateTransferOfferOrConfirm(Message rxMsg, boolean offer) {

        if (Network.clock() >= node.net.maxRounds) {
            // do not schedule any transfer offers in the last round
            return;
        }

        Message msg = new Message((offer ? Message.Type.OFFER : Message.Type.CONFIRM),
                rxMsg.vnf);

        List<Flow> rxFlows = rxMsg.getFlowGroup().getFlows();
        FlowNode fnode;
        double time = Network.clock();

        msg.srcNodeId = node.getNodeId();
        msg.dstNodeId = rxMsg.srcNodeId;
        msg.link = link;
        msg.getFlowGroup().getFlows().addAll(rxFlows);

        if (offer) {

            // offers should not be generated more frequently than 
            // one in N timesteps (temporarily, N = 10)
            for (Flow f : rxFlows) {
                fnode = node.getFlowNodeByFlowId(f.getFlowId());

                if (fnode.egressPort.equals(this)) {

                    if ((fnode.usOfferPending && (time - fnode.usOfferTimestamp) < 10.0)
                            || ((fnode.findSfcFragmentSize() == 1) && fnode.dsOfferPending == true
                            && (time - fnode.dsOfferTimestamp) < 10.0)) {

                        // it has NOT been long enough to generate another offer
                        info("Avoiding DUPLICATE Transfer Offer in u/s direction at time " + time
                                + " for fnode " + fnode.toString() + " and VNF " + rxMsg.vnf.toString());
                        return;
                    }

                    fnode.usOfferPending = true;
                    fnode.usOfferTimestamp = time;
                } else {
                    if ((fnode.dsOfferPending && (time - fnode.dsOfferTimestamp) < 10.0)
                            || ((fnode.findSfcFragmentSize() == 1) && fnode.usOfferPending == true
                            && (time - fnode.usOfferTimestamp) < 10.0)) {
                        info("Avoiding DUPLICATE Transfer Offer in d/s direction at time " + time
                                + " for fnode " + fnode.toString() + " and VNF " + rxMsg.vnf.toString());
                        // it has NOT been 5 time steps since offer was generated
                        return;
                    }
                    fnode.dsOfferPending = true;
                    fnode.dsOfferTimestamp = time;
                }
            }
        }

        // INFO
        warning("Transmitting a Transfer " + (offer ? "OFFER " : "CONFIRM ") + "for " + rxMsg.vnf.toString()
                + " from N" + msg.srcNodeId + " to N" + rxMsg.srcNodeId + " at time "
                + Network.clock() + (offer ? ("; neighbor's max utility = " + rxMsg.maxUtility) : ""));

        // CONFIRM messages are sent instantaneously to the neighbor
        double delay = (offer ? 1.0 : 0.0);

        // SADHANA
//        node.sim_schedule(getSimPort(), delay,
//                (offer ? Message.Type.OFFER : Message.Type.CONFIRM), msg);
        transmitMsg((offer ? Message.Type.OFFER : Message.Type.CONFIRM), msg, delay);

    }

    public void forwardTransferOfferOrConfirm(Message msg) {

        info("Forwarding a " + (msg.type == Message.Type.OFFER ? "OFFER " : "CONFIRM ")
                + "at Port " + toString());

        double delay = 0.0;

        Sim_port port = getSimPort();

        if (port == null) {
            System.out.println("TRANSFER OFFER OR CONFIRM ON HOST PORT at port " + toString()
                    + " on Node " + node.toString());
            System.out.println(msg.toString());
            return;
        }
// SADHANA
        // node.sim_schedule(getSimPort(), delay, msg.type, msg);
        transmitMsg(msg.type, msg, delay);
        
    }

    private int aggregateTransferRequests(Map<Integer, FlowGroup> trMap, VNF vnf,
            List<FlowNode> flowNodes, boolean ingressFlows) {

//        info("aggregateTransferRequests at " + toString());
        FlowGroup fg; // flow group to be added
        int minDifferential = 9999999, differential = 0;
        int maxUtility = 0;
        int srcNodeId = 0;

        if (flowNodes == null) {
            // no flow nodes to process
            return minDifferential;
        }

        for (FlowNode fn : flowNodes) {
            fn.info(fn.toString());

            // u/s differential is differential transmitted FROM u/s node
            // this should be transmitted to egress flows
            if (fn.usDifferential != 0 && fn.usVnf.equals(vnf) && !ingressFlows) {
                // there is a localDifferential force active on this FN
                srcNodeId = fn.usDiffNodeId;
                differential = fn.usDifferential;
                maxUtility = fn.usMaxUtility;

                fn.info("Upstream differential found for " + fn.toString()
                        + " for VNF " + vnf.toString() + "; Src Node = N"
                        + srcNodeId + "; differential = " + differential
                        + "; max utility = " + maxUtility);
            }

            if (fn.dsDifferential != 0 && fn.dsVnf.equals(vnf) && ingressFlows) {
                srcNodeId = fn.dsDiffNodeId;
                differential = fn.dsDifferential;
                maxUtility = fn.dsMaxUtility;

                fn.info("Downstream differential found for " + fn.toString()
                        + " for VNF " + vnf.toString() + "; Src Node = N"
                        + srcNodeId + "; differential = " + differential
                        + "; max utility = " + maxUtility);
            }

            if (srcNodeId == 0) {
                // neither u/s nor d/s differential detected at this flow node for this VNF
                continue;
            }

            if (srcNodeId == neighbor.getNodeId()) {
                // Do NOT forward a TR from a neighbor
                // back to the same neighbor
                continue;
            }

            if (!trMap.containsKey(srcNodeId)) {
                fg = new FlowGroup();
                fg.differential = differential;
                fg.maxUtility = maxUtility;
                if (minDifferential > differential) {
                    minDifferential = differential;
                }

                trMap.put(srcNodeId, fg);
            } else {
                fg = trMap.get(srcNodeId);
            }

            fn.info("Adding flow node " + fn.toString()
                    + " to flow group for " + vnf.toString() + " at " + toString());
            fg.addFlow(fn.getFlow());
        }

        return minDifferential;
    }

    public void prettyPrint() {
        if (type == Port.PORT_TYPE_HOST) {
            System.out.print("HOST ");
        } else {
            System.out.print(neighbor.toString() + "[Port " + getPortId() + "]" + " ");
        }

        prettyPrintForceMap("InPull :: ", ingressPullMap);

        prettyPrintForceMap("EgPull :: ", egressPullMap);

        prettyPrintForceMap("InPush :: ", ingressPushMap);

        prettyPrintForceMap("EgPush :: ", egressPushMap);

    }

    public void prettyPrintForceMap(String header, Map<VNF, List<FlowNode>> forceMap) {

        for (VNF vnf : VNF.getVnfList()) {
            List<FlowNode> fnodeL = forceMap.get(vnf);
            if (fnodeL == null) {
                continue;
            }

            if (fnodeL.size() == 0) {
                continue;
            }

            System.out.print(header);
            System.out.print(vnf.toString());
            for (FlowNode fn : fnodeL) {
                System.out.print("::" + fn.toString());
            }
        }

        System.out.print(" ");
    }

    public String toString() {
        if (node == null) {
            return "Unknown Port";
        }
        if (neighbor == null) {
            return "HOST port";
        }
        return "Port " + node.toString() + "-" + neighbor.toString();
    }

    public void setDebugFlag(boolean val) {
        this.debug = val;
    }

    public void info(String msg) {
        if (logger.isLoggable(Level.INFO) || debug) {
            System.out.println(toString() + " " + msg);
        }
    }

    public void warning(String msg) {
        if (logger.isLoggable(Level.WARNING)) {
            System.out.println("WARNING: " + msg);
        }
    }

}
