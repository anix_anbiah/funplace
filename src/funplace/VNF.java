/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import static funplace.Network.info;
import java.util.Map;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author anix- A Virtual Network Function
 */
public class VNF {

    static Map<Integer, VNF> vnfs = new HashMap<>();
    int vnfId;
    String vnfName;

    int staticCost;
    int dynamicCost;
    
    protected static final int DEFAULT_VNF_DYNAMIC_COST = 10;
    protected static final int DEFAULT_VNF_STATIC_COST = 1000;


    public static final int STAR = 1;
    public static final int SQUARE = 2;
    public static final int CIRCLE = 3;
    public static final int TRIANGLE = 4;
    public static final int HEART = 5;
    public static final int DIAMOND = 6;
    public static final int SPADE = 7;
    public static final int OVAL = 8;
    //    public static final int CLOVER = 9;
//    public static final int RING = 10;
//    public static final int RHOMBUS = 11;
//    public static final int RECTANBLE = 12;

    public static final String[] VNF_NAME = {"ZERO", "STAR", "SQUARE", "CIRCLE",
        "TRIANGLE", "HEART", "DIAMOND", "SPADE", "OVAL"
    };
    
    protected static void createVNFs() {
        VNF vnf;
        // create all the VNFs
        for (int i = 1; i < VNF.VNF_NAME.length; i++) {
            vnf = new VNF(i, VNF.DEFAULT_VNF_STATIC_COST, VNF.DEFAULT_VNF_DYNAMIC_COST);
//            info("Creating VNF " + vnf.toString());
        }
    }

//    public static final String[] VNF_NAME= {"ZERO", "STAR", "SQUARE", "CIRCLE",
//        "TRIANGLE", "DIAMOND", "HEART", "SPADE", "CLOVER", "OVAL", "RING", 
//        "RHOMBUS", "RECTANGLE"
//    };
    public VNF(int vnfId, int staticCost, int dynamicCost) {

        this.vnfId = vnfId;

        assert (vnfId <= VNF_NAME.length);

        this.vnfName = VNF_NAME[vnfId];

        this.staticCost = staticCost;
        this.dynamicCost = dynamicCost;

        vnfs.put(vnfId, this);
    }

    public static VNF getVNFbyId(int vnfId) {
        assert (vnfId <= vnfs.size());

        return vnfs.get(vnfId);
    }

    public String toString() {
        return vnfName;
    }

    public static String getNameById(int vnfId) {
        return VNF_NAME[vnfId];
    }

    static Collection<VNF> getVnfList() {
        return vnfs.values();
    }

    public int getVnfId() {
        return vnfId;
    }

    public int getStaticCost() {
        return staticCost;
    }

    public int getDynamicCost() {
        return dynamicCost;
    }
    
    @Override
    public boolean equals(Object o) {

        if(getVnfId() == ((VNF) o).getVnfId()) {
            return true;
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.vnfId;
        return hash;
    }

}
