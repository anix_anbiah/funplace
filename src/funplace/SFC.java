/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author anix - a Service Function Chain
 *
 */
public class SFC {

    protected static final int SFC_LEN_3 = 3;
    protected static final int SFC_LEN_5 = 5;
    protected static final int SFC_LEN_8 = 8;

    protected static final int SFC_ST_CI_SQ = 1;
    protected static final int SFC_HT_CI_SQ = 2;
    protected static final int SFC_CI_TR = 3;
    protected static final int SFC_FIVE_LONG = 4;
    protected static final int SFC_SEVEN_LONG = 5;
    protected static final int SFC_EIGHT_LONG = 6;
    protected static final int[] SFC_LONG_HETERO = {7, 8, 9, 10, 11, 12, 13, 14, 15};

    protected static final String[] SFCNames = {"ZERO", "STAR-CIRCLE-SQUARE", "HEART-CIRCLE-SQUARE",
        "CIRCLE-TRIANGLE", "SFC_FIVE_LONG", "SFC_SEVEN_LONG", "SFC_EIGHT_LONG", "SFC_HETERO-1", "SFC_HETERO-2",
        "SFC_HETERO-3", "SFC_HETERO-4", "SFC_HETERO-5", "SFC_HETERO-6", "SFC_HETERO-7", "SFC_HETERO-8"};

    protected static SFC sfcStCiSq, sfcHtCiSq, sfcCiTr, sfcFiveLong, sfcSevenLong, sfcEightLong;
    protected static SFC[] heteroSfcs;

    protected static final int[][] sfcLongHetero_VNFs = {
        {VNF.CIRCLE, VNF.DIAMOND, VNF.SQUARE, VNF.OVAL,
            VNF.HEART, VNF.SPADE, VNF.STAR, VNF.TRIANGLE}, // FIRST HETERO SFC
        {VNF.CIRCLE, VNF.SQUARE, VNF.DIAMOND, VNF.HEART,
            VNF.OVAL, VNF.STAR, VNF.TRIANGLE, VNF.SPADE}, // SECOND HETERO SFC
        {VNF.OVAL, VNF.STAR, VNF.TRIANGLE, VNF.SPADE,
            VNF.CIRCLE, VNF.SQUARE, VNF.DIAMOND, VNF.HEART}, // THIRD HETERO SFC
        {VNF.DIAMOND, VNF.CIRCLE, VNF.OVAL, VNF.SQUARE,
            VNF.SPADE, VNF.HEART, VNF.TRIANGLE, VNF.STAR}, // FOURTH HETERO SFC
        {VNF.SQUARE, VNF.CIRCLE, VNF.HEART, VNF.DIAMOND,
            VNF.STAR, VNF.OVAL, VNF.SPADE, VNF.TRIANGLE}, // FIFTH HETERO SFC
        {VNF.STAR, VNF.TRIANGLE, VNF.CIRCLE, VNF.DIAMOND,
            VNF.SQUARE, VNF.OVAL, VNF.HEART, VNF.SPADE}, // SIXTH HETERO SFC
        {VNF.SPADE, VNF.TRIANGLE, VNF.STAR, VNF.OVAL,
            VNF.HEART, VNF.DIAMOND, VNF.SQUARE, VNF.CIRCLE}, // SEVENTH HETERO SFC
        {VNF.OVAL, VNF.STAR, VNF.SQUARE, VNF.CIRCLE,
            VNF.HEART, VNF.DIAMOND, VNF.TRIANGLE, VNF.SPADE}, // EIGHTH HETERO SFC
    };

    static Map<Integer, SFC> sfcs = new HashMap<>();

    // list of VNFs in the SFC
    List vnfs;
    int sfcId;
    String name;

    // give special index values for the start of the SFC and
    // the end of the SFC
    public static final int BEGIN_MARKER = -1;
    public static final int END_MARKER = 1024;

    public SFC(int sfcId, String name) {
        this.sfcId = sfcId;
        this.name = name;
        vnfs = new ArrayList();
    }

    public void addVNF(VNF vnf) {
        vnfs.add(vnf);
    }

    public static SFC getSFCbyId(int sfcId) {
        return sfcs.get(sfcId);
    }

    protected static void createSFCs() {
        // create STAR-CIRCLE-SQUARE
        sfcStCiSq = new SFC(SFC_ST_CI_SQ, SFCNames[SFC_ST_CI_SQ]);
        sfcStCiSq.addVNF(VNF.getVNFbyId(VNF.STAR));
        sfcStCiSq.addVNF(VNF.getVNFbyId(VNF.CIRCLE));
        sfcStCiSq.addVNF(VNF.getVNFbyId(VNF.SQUARE));

        sfcHtCiSq = new SFC(SFC_HT_CI_SQ, SFCNames[SFC_HT_CI_SQ]);
        sfcHtCiSq.addVNF(VNF.getVNFbyId(VNF.HEART));
        sfcHtCiSq.addVNF(VNF.getVNFbyId(VNF.CIRCLE));
        sfcHtCiSq.addVNF(VNF.getVNFbyId(VNF.SQUARE));

        sfcCiTr = new SFC(SFC_CI_TR, SFCNames[SFC_CI_TR]);
        sfcCiTr.addVNF(VNF.getVNFbyId(VNF.CIRCLE));
        sfcCiTr.addVNF(VNF.getVNFbyId(VNF.TRIANGLE));

        sfcFiveLong = new SFC(SFC_FIVE_LONG, SFCNames[SFC_FIVE_LONG]);
        sfcFiveLong.addVNF(VNF.getVNFbyId(VNF.CIRCLE));
        sfcFiveLong.addVNF(VNF.getVNFbyId(VNF.SQUARE));
        sfcFiveLong.addVNF(VNF.getVNFbyId(VNF.STAR));
        sfcFiveLong.addVNF(VNF.getVNFbyId(VNF.TRIANGLE));
        sfcFiveLong.addVNF(VNF.getVNFbyId(VNF.DIAMOND));

        sfcSevenLong = new SFC(SFC_SEVEN_LONG, SFCNames[SFC_SEVEN_LONG]);
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.CIRCLE));
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.SQUARE));
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.STAR));
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.TRIANGLE));
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.DIAMOND));
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.HEART));
        sfcSevenLong.addVNF(VNF.getVNFbyId(VNF.SPADE));

        sfcEightLong = new SFC(SFC_EIGHT_LONG, SFCNames[SFC_EIGHT_LONG]);
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.CIRCLE));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.SQUARE));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.STAR));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.TRIANGLE));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.DIAMOND));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.HEART));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.SPADE));
        sfcEightLong.addVNF(VNF.getVNFbyId(VNF.OVAL));

        heteroSfcs = new SFC[8];
        for (int i = 0; i < 8; i++) {
            heteroSfcs[i] = new SFC(SFC_LONG_HETERO[i], SFCNames[SFC_LONG_HETERO[i]]);
            for (int j = 0; j < 8; j++) {
                heteroSfcs[i].addVNF(VNF.getVNFbyId(sfcLongHetero_VNFs[i][j]));
            }

//            System.out.println("Create Hetero SFC " + heteroSfcs[i].toString());
        }
    }

    public List<VNF> getVnfList() {
        return vnfs;
    }
    
    protected int getLength() {
        return vnfs.size();
    }

    public VNF getPreviousVNF(VNF vnf) {
        int index = vnfs.indexOf(vnf);

        index -= 1;

        if (index < 0) {
            return null;
        } else {
            return (VNF) vnfs.get(index);
        }
    }

    public int getPreviousIndex(int index) {
        if (index == BEGIN_MARKER) {
            // cannot go any farther
            return BEGIN_MARKER;
        }

        if (index == END_MARKER) {
            return vnfs.size() - 1; // index of the last VNF 
        }

        if (index == 0) {
            // index is already pointing to the first index
            return BEGIN_MARKER;
        }

        // else just decrement index
        return index - 1;
    }

    public int getNextIndex(int index) {
        if (index == END_MARKER) {
            // already at the end of the list- cannot go any farther
            return END_MARKER;
        }

        if (index == (vnfs.size() - 1)) {
            // index is pointing to the end of the list
            return END_MARKER;
        }

        if (index == BEGIN_MARKER) {
            return 0; // index of the first VNF
        }

        // else just increment the index
        return index + 1;
    }

    public VNF getNextVNF(VNF vnf) {
        int index = vnfs.indexOf(vnf);

        if (index == -1) {
            return null;
        }

        index += 1;

        if (index >= vnfs.size()) {
            return null;
        } else {
            return (VNF) vnfs.get(index);
        }

    }

    public int indexOf(VNF vnf) {

        if (vnf == null) {
            return -1;
        }

        return vnfs.indexOf(vnf);
    }

    public VNF getVnfByIndex(int index) {
        return (VNF) vnfs.get(index);
    }

    public int getFirstIndex() {
        return 0;
    }

    public int getLastIndex() {
        return vnfs.size() - 1;
    }

    public static String indexToString(int index) {
        if (index == SFC.BEGIN_MARKER) {
            return "BGN";
        }

        if (index == SFC.END_MARKER) {
            return "END";
        }

        return "" + index;
    }

    public void prettyPrint(int previousIndex, int nextIndex) {
        int start, end;

        if ((previousIndex == getLastIndex())
                || (nextIndex == getFirstIndex())
                || (nextIndex == (previousIndex + 1))) {
            System.out.print("NONE");
            return;
        }

        start = getNextIndex(previousIndex);
        end = getPreviousIndex(nextIndex);

        for (int i = start;
                i <= end;
                i++) {
            System.out.print(vnfs.get(i).toString() + " ");
        }

    }

    public String toString() {
        String str = "";
        for (int i = 0;
                i < vnfs.size();
                i++) {
            str += (vnfs.get(i).toString() + " ");
        }
        return str;
    }

}
