/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

/**
 *
 * @author anix
 */
public class TrashCan {
    
    /*

    // unit test for DFP
    protected void addTopoAndInitialFlows() {

        if ("dcn".equals(topo)) {
            System.out.println("Testing network with DCN topology "
                    + "k = " + topoArg1 + "; h = " + topoArg2 + "; num flows = "
                    + numFlows);
            testDcnTopology(topoArg1, topoArg2);
            return;
        }

        unitTestId = 0;

        if (unitTestId == 1) {
            testNetworkOne(); // ring with wings
            return;
        }

        if (unitTestId == 2) {
            testNetworkTwo(); // 'V' with n5 as vertex
            return;
        }

        if (unitTestId == 3) {
            testNetworkThree(); // 'Y' with n3 as junction and n4 as tail; n5 is adjacent to n4
            return;
        }

        if (unitTestId == 4) {
            testNetworkFour(); // n4 is junction with 3 prongs- n1, n2, n3; n5 & n6 form a tail
            return;
        }

        if (unitTestId == 5) {
            testNetworkFive(); // test ECMP routing- n1 and n2 are two nodes
            // connected by 5 equal cost (2-hop) paths through n3, n4, n5, n6 and n7
        }

        if (unitTestId == 6) {
            testNetworkSix(); // test ECMP routing- DCN network
        }

        if (unitTestId == 7) {
            testNetworkSeven(); // test ECMP routing- DCN network
        }

        if (unitTestId == 8) {
            testDcnTopology(16, 3); // test on a k-ary Fat tree topology
        }

        info("Unit test completed");
    }
    
    protected void testNetworkOne() {

        //create a few sample nodes
        Node n1, n2, n3, n4, n5, n6;

        n1 = net.createNode();
        n2 = net.createNode();
        n3 = net.createNode();
        n4 = net.createNode();
        n5 = net.createNode();
        n6 = net.createNode();

        // N1 and N2 have a linear connection
        net.addEdge(n1, n2);

        // add links between nodes- N2-N5 form a 4-node ring
        net.addEdge(n2, n3);
        net.addEdge(n2, n4);
        net.addEdge(n3, n5);
        net.addEdge(n4, n5);

        // N5 and N6 are two nodes off N
        net.addEdge(n5, n6);

        net.createKsp();

        net.createFlowBetweenNodes(n1, n6, SFC.sfcStCiSq, 100); // we will add SFC requirements later
        net.createFlowBetweenNodes(n3, n4, SFC.sfcCiTr, 100);
        net.createFlowBetweenNodes(n2, n5, SFC.sfcHtCiSq, 100);
    }

    protected void unitTestCheck() {
        if (unitTestId == 1) {
            //    unitTestCheckOne();
        }

        if (unitTestId == 2) {
            unitTestCheckTwo();
        }

        if (unitTestId == 3) {
            unitTestCheckThree();
        }
    }

    protected void unitTestCheckOne() {
        Node n1 = net.getNodeByNodeId(1);
        Node n2 = net.getNodeByNodeId(2);
        Node n3 = net.getNodeByNodeId(3);
        Node n4 = net.getNodeByNodeId(4);
        Node n5 = net.getNodeByNodeId(5);

        Flow f1 = net.getFlowByFlowId(1);
        Flow f2 = net.getFlowByFlowId(2);
        Flow f3 = net.getFlowByFlowId(3);

        n1.utFlowNodeCheck(f1, VNF.STAR); // check if CIRCLE is on N1 for F1
        n3.utFlowNodeCheck(f1, VNF.CIRCLE);
        n5.utFlowNodeCheck(f1, VNF.SQUARE);

        n3.utFlowNodeCheck(f2, VNF.CIRCLE);
        n2.utFlowNodeCheck(f2, VNF.TRIANGLE);

        n2.utFlowNodeCheck(f3, VNF.HEART);
        n3.utFlowNodeCheck(f3, VNF.CIRCLE);
        n5.utFlowNodeCheck(f3, VNF.SQUARE);

        System.out.println("Unit test #1 completed");
    }

    protected void testNetworkTwo() {

        //create a few sample nodes
        Node n1, n2, n3, n4, n5;

        n1 = net.createNode();
        n2 = net.createNode();
        n3 = net.createNode();
        n4 = net.createNode();
        n5 = net.createNode();

        // create a V-shaped network with n5 as the vertex
        net.addEdge(n1, n2);
        net.addEdge(n2, n5); // that's one arm of the "V"

        net.addEdge(n3, n4);
        net.addEdge(n4, n5); // the other arm of the "V"

        net.createKsp();

        net.createFlowBetweenNodes(n1, n5, SFC.sfcSevenLong, 100);
        net.createFlowBetweenNodes(n3, n5, SFC.sfcSevenLong, 100);

    }

    protected void unitTestCheckTwo() {

        Node n1 = net.getNodeByNodeId(1);
        Node n2 = net.getNodeByNodeId(2);
        Node n3 = net.getNodeByNodeId(3);
        Node n4 = net.getNodeByNodeId(4);
        Node n5 = net.getNodeByNodeId(5);

        Flow f1 = net.getFlowByFlowId(1);
        Flow f2 = net.getFlowByFlowId(2);

        n5.utFlowNodeCheck(f1, VNF.TRIANGLE);
        n5.utFlowNodeCheck(f2, VNF.TRIANGLE);

        n5.utFlowNodeCheck(f1, VNF.CIRCLE);
        n5.utFlowNodeCheck(f2, VNF.CIRCLE);

        System.out.println("Unit test #2 completed");
    }

    protected void testNetworkThree() {

        //create a few sample nodes
        Node n1, n2, n3, n4, n5; // n1, n2, n3, n4 form a "Y" with n3 as the junction
        // n5 is an adjacent node to n4

        n1 = net.createNode();
        n2 = net.createNode();
        n3 = net.createNode();
        n4 = net.createNode();
        n5 = net.createNode();

        // create a V-shaped network with n5 as the vertex
        net.addEdge(n1, n3); // one arm of the "Y"
        net.addEdge(n2, n3); // other arm of the "Y"

        net.addEdge(n4, n3); // tail of the "Y"
        net.addEdge(n5, n4); // additional neighbor of n4

        net.createKsp();

        net.createFlowBetweenNodes(n1, n4, SFC.sfcCiTr, 100);
        net.createFlowBetweenNodes(n2, n4, SFC.sfcCiTr, 100);
        net.createFlowBetweenNodes(n5, n4, SFC.sfcCiTr, 10);
        // we are expecting n4 to attract all the VNFs due to max utility

    }

    private void unitTestCheckThree() {
        Node n1 = net.getNodeByNodeId(1);
        Node n2 = net.getNodeByNodeId(2);
        Node n3 = net.getNodeByNodeId(3);
    }

    protected void testNetworkFour() {
        //create a few sample nodes
        Node n1, n2, n3, n4, n5, n6; // n4 is the junction of 3 "prongs" n1, n2 and n3
        // n5 is an adjacent node to n4

        n1 = net.createNode();
        n2 = net.createNode();
        n3 = net.createNode();
        n4 = net.createNode();
        n5 = net.createNode();
        n6 = net.createNode();

        // create a V-shaped network with n5 as the vertex
        net.addEdge(n1, n4); // first prong
        net.addEdge(n2, n4); // second prong
        net.addEdge(n3, n4); // third prong

        net.addEdge(n4, n5); // the tail
        net.addEdge(n6, n5);

        net.createKsp();

        net.createFlowBetweenNodes(n1, n5, SFC.sfcHtCiSq, 100);
        net.createFlowBetweenNodes(n2, n5, SFC.sfcHtCiSq, 100);
        net.createFlowBetweenNodes(n3, n5, SFC.sfcHtCiSq, 10);
        net.createFlowBetweenNodes(n6, n5, SFC.sfcHtCiSq, 10);
        // we are expecting n4 to attract all the VNFs due to max utility

    }

    protected void testNetworkFive() {

        //creates a network that tests ECMP routing
        // n1 and n2 are two nodes at the extremes that are connected
        // by several parallel paths with n3, n4, n5, .. etc being intermediate
        // nodes
        Node n1, n2, n3, n4, n5, n6, n7;

        n1 = net.createNode();
        n2 = net.createNode();
        n3 = net.createNode();
        n4 = net.createNode();
        n5 = net.createNode();
        n6 = net.createNode();
        n7 = net.createNode();

        // n1 is connected to all the intermediate nodes
        net.addEdge(n1, n3);
        net.addEdge(n1, n4);
        net.addEdge(n1, n5);
        net.addEdge(n1, n6);
        net.addEdge(n1, n7);

        // so is n2
        net.addEdge(n2, n3);
        net.addEdge(n2, n4);
        net.addEdge(n2, n5);
        net.addEdge(n2, n6);
        net.addEdge(n2, n7);

        net.createKsp();

        // create twenty flows, all between n1 and n2
        for (int i = 0; i < 20; i++) {
            net.createFlowBetweenNodes(n1, n2, SFC.sfcCiTr, 100);
        }
    }

    protected void testNetworkSix() {
        // a DCN network with 2 core, 4 aggregator switches (2 levels)

        Node n1, n2, n11, n12, n21, n22;
        Link l;
        Flow f;

        n1 = net.createNodeWithId(1);
        n2 = net.createNodeWithId(2);

        n11 = net.createNodeWithId(11);
        n12 = net.createNodeWithId(12);

        n21 = net.createNodeWithId(21);
        n21.setDebugFlag(true);

        n22 = net.createNodeWithId(22);

        net.addEdge(n11, n1);
        net.addEdge(n12, n1);

        l = (Link) net.addEdge(n21, n2);
        l.setDebugAtPort(n21);

        net.addEdge(n22, n2);

        net.addEdge(n11, n2);
        net.addEdge(n12, n2);
        net.addEdge(n21, n1);
        net.addEdge(n22, n1);

        net.createKsp();

        f = net.createFlowBetweenNodes(n11, n21, SFC.sfcSevenLong, 100);
        f.setDebugFlag(true);

        net.createFlowBetweenNodes(n21, n11, SFC.sfcSevenLong, 100);
        net.createFlowBetweenNodes(n12, n22, SFC.sfcSevenLong, 100);

//        for (int i = 0; i < 2; i++) {
//            net.createFlowBetweenNodes(11, 21, sfcCiTr, 100);
//            net.createFlowBetweenNodes(11, 22, sfcCiTr, 100);
//            net.createFlowBetweenNodes(21, 11, sfcCiTr, 100);
//            net.createFlowBetweenNodes(21, 12, sfcCiTr, 100);
//        }
    }

    protected void testNetworkSeven() {
        // a DCN network with 2 core, 4 aggregator and 8 ToR switches (3 levels)

        Node n1, n2, n3, n11, n12, n21, n22, n31, n32;
        Node n111, n112, n121, n122;
        Node n211, n212, n221, n222;
        Node n311, n312, n321, n322;

        SFC sfc;

        n1 = net.createNodeWithId(1, Node.Type.CORE);
        n2 = net.createNodeWithId(2, Node.Type.CORE);
        n3 = net.createNodeWithId(3, Node.Type.CORE);

        n11 = net.createNodeWithId(11, Node.Type.AGGR);
        n12 = net.createNodeWithId(12, Node.Type.AGGR);

        n21 = net.createNodeWithId(21, Node.Type.AGGR);
        n22 = net.createNodeWithId(22, Node.Type.AGGR);

        n31 = net.createNodeWithId(31, Node.Type.AGGR);
        n32 = net.createNodeWithId(32, Node.Type.AGGR);

        n111 = net.createNodeWithId(111, Node.Type.TOR);
        n112 = net.createNodeWithId(112, Node.Type.TOR);
        n121 = net.createNodeWithId(121, Node.Type.TOR);
        n122 = net.createNodeWithId(122, Node.Type.TOR);

        n211 = net.createNodeWithId(211, Node.Type.TOR);
        n212 = net.createNodeWithId(212, Node.Type.TOR);
        n221 = net.createNodeWithId(221, Node.Type.TOR);
        n222 = net.createNodeWithId(222, Node.Type.TOR);

        n311 = net.createNodeWithId(311, Node.Type.TOR);
        n312 = net.createNodeWithId(312, Node.Type.TOR);
        n321 = net.createNodeWithId(321, Node.Type.TOR);
        n322 = net.createNodeWithId(322, Node.Type.TOR);

        net.addEdge(n11, n1);
        net.addEdge(n12, n1);

        net.addEdge(n11, n2);
        net.addEdge(n12, n2);

        net.addEdge(n11, n3);
        net.addEdge(n12, n3);

        net.addEdge(n21, n1);
        net.addEdge(n22, n1);

        net.addEdge(n21, n2);
        net.addEdge(n22, n2);

        net.addEdge(n21, n3);
        net.addEdge(n22, n3);

        net.addEdge(n31, n1);
        net.addEdge(n32, n1);

        net.addEdge(n31, n2);
        net.addEdge(n32, n2);

        net.addEdge(n31, n3);
        net.addEdge(n32, n3);

        net.addEdge(n111, n11);
        net.addEdge(n111, n12);
        net.addEdge(n112, n11);
        net.addEdge(n112, n12);

        net.addEdge(n121, n11);
        net.addEdge(n121, n12);
        net.addEdge(n122, n11);
        net.addEdge(n122, n12);

        net.addEdge(n211, n21);
        net.addEdge(n211, n22);
        net.addEdge(n212, n21);
        net.addEdge(n212, n22);

        net.addEdge(n221, n21);
        net.addEdge(n221, n22);
        net.addEdge(n222, n21);
        net.addEdge(n222, n22);

        net.addEdge(n311, n31);
        net.addEdge(n311, n32);
        net.addEdge(n312, n31);
        net.addEdge(n312, n32);

        net.addEdge(n321, n31);
        net.addEdge(n321, n32);
        net.addEdge(n322, n31);
        net.addEdge(n322, n32);

        net.createDsp();
        net.createKsp();

        int flowNum = 0;
        int maxFlows = 20;

        sfc = net.sfcLen == SFC.SFC_LEN_8 ? SFC.sfcEightLong
                : (net.sfcLen == SFC.SFC_LEN_5 ? SFC.sfcFiveLong : SFC.sfcHtCiSq);

        for (Node src : net.getNodes().values()) {
            for (Node dst : net.getNodes().values()) {
                if (src.equals(dst)) {
                    continue;
                }

                if (src.type != Node.Type.TOR) {
                    continue;
                }
                if (dst.type != Node.Type.TOR) {
                    continue;
                }

                if (net.sfcHetero) {
                    sfc = SFC.heteroSfcs[flowNum % 8];
                }

                if (flowNum++ >= maxFlows) {
                    return; // DONE
                }

//               System.out.println("Creating flow with SFC " + sfc.toString());
                net.createFlowBetweenNodes(src, dst, sfc, DEFAULT_FLOW_RATE);

            }
        }

    } 

    private void testDcnTopology(int numCore, int numAggr, int numTor) {
        // a DCN network with 2 core, 4 aggregator and 8 ToR switches (3 levels)

        SFC sfc;

        coreSwitches = new ArrayList<>();
        aggrSwitches = new ArrayList<>();
        torSwitches = new ArrayList<>();
        torSwitchesClone = new ArrayList<>();

        Node coreSw, aggrSw, torSw;
        int nodeId;

        for (int core = 1; core <= numCore; core++) {
            //create the core switch;

            coreSw = net.createNodeWithId(core, Node.Type.CORE);

            coreSwitches.add(coreSw);

            for (int aggr = 1; aggr <= numAggr; aggr++) {
                // create the aggregator switches

                nodeId = core * 10 + aggr;
                aggrSw = net.createNodeWithId(nodeId, Node.Type.AGGR);

                aggrSwitches.add(aggrSw);

                for (int tor = 1; tor <= numTor; tor++) {

                    nodeId = core * 100 + aggr * 10 + tor;
                    torSw = net.createNodeWithId(nodeId, Node.Type.TOR);

                    torSwitches.add(torSw);
                    torSwitchesClone.add(torSw);
                }
            }
        }

        // now add the links between the switches
        Iterator torItr, aggrItr, coreItr;

        // every ToR switch is connected to each aggregator switch
        torItr = torSwitches.iterator();
        while (torItr.hasNext()) {
            torSw = (Node) torItr.next();

            aggrItr = aggrSwitches.iterator();
            while (aggrItr.hasNext()) {
                aggrSw = (Node) aggrItr.next();

                net.addEdge(torSw, aggrSw);
            }

        }

        // every Aggregator switch is connected to each Core switch
        aggrItr = aggrSwitches.iterator();
        while (aggrItr.hasNext()) {

            aggrSw = (Node) aggrItr.next();

            coreItr = coreSwitches.iterator();
            while (coreItr.hasNext()) {

                coreSw = (Node) coreItr.next();

                net.addEdge(aggrSw, coreSw);
            }
        }

        net.createDsp();
        net.createKsp();

        int numFlows = 0;
        int maxFlows = 8;

        sfc = net.sfcLen == SFC.SFC_LEN_8 ? SFC.sfcEightLong
                : (net.sfcLen == SFC.SFC_LEN_5 ? SFC.sfcFiveLong : SFC.sfcHtCiSq);

        Iterator torSrcItr = torSwitches.iterator();

        Node src, dst;

        while (torSrcItr.hasNext()) {

            src = (Node) torSrcItr.next();
            Iterator torDstItr = torSwitchesClone.iterator();

            while (torDstItr.hasNext()) {

                dst = (Node) torDstItr.next();

                if (src.equals(dst)) {
                    continue;
                }

                if (net.sfcHetero) {
                    sfc = SFC.heteroSfcs[numFlows % 8];
                }

                if (numFlows++ >= maxFlows) {
                    return; // DONE
                }

                net.createFlowBetweenNodes(src, dst, sfc, DEFAULT_FLOW_RATE);

            }
        }

    }
    
    */
}
