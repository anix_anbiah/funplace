/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author anix- an instance of a VNF at a node
 */
public class VNFNode {

    Node node; //local node
    VNF vnf;
    int currentUtility;
    int maxUtility;

    List<Flow> flows; // list of flows using this VNF Node

    public VNFNode(Node node, VNF vnf) {
        this.node = node;
        this.vnf = vnf;
        this.currentUtility = 0;
        this.maxUtility = 0;

        this.flows = new ArrayList<>();

        node.info("Creating a VNF node in " + node.toString() + " for VNF " + vnf.toString());
    }

    @Override
    public String toString() {
        return ("VNF Node " + node.toString() + "-" + vnf.toString());
    }

    public int getCurrentUtility() {
        return currentUtility;
    }

    public int getMaxUtility() {
        return maxUtility;
    }

    public void addFlow(Flow flow) {
        flows.add(flow);
        this.currentUtility += flow.getBitRate();
    }
    
    public void removeFlow(Flow flow) {
        flows.remove(flow);
        this.currentUtility -= flow.getBitRate();
    }

    // Differential is the difference between the current utility of the VNF
    // and the potential max utility
    public int getDifferential() {
        return maxUtility - currentUtility;
    }

    public void resetMaxUtility() {
        Network.info("Resetting max utility for VNF Node" + toString());
        this.maxUtility = 0;
    }

    public void updateMaxUtility() {

        // first, initialize max utility to current utility
        this.maxUtility = currentUtility;
        
//        Network.info("updateMaxUtility " + toString());

        for (Port port : node.getPorts().values()) {

            if (port.type == Port.PORT_TYPE_HOST) {
                // skip over HOST ports
                continue;
            }

            // find the potential increase in utility of the VNF
            // by pulling from upstream and downstream neighbors for all
            // flows on a given port.
            int portDifferential = port.findDifferential(this.vnf);

            this.maxUtility += portDifferential;
        }
        if (maxUtility != currentUtility) {
            Network.info("updateMaxUtility at VNF Node " + toString() + "; current utility = " + currentUtility
                    + "; max utility = " + maxUtility + "; clock " + Network.clock());
        }
    }

    // This method is called after the incoming TRs have been handled
    // and differential has been calculated for the VNF Node.
    // Now walk through each port and generate transfer requests based on 
    // either (i) transferred forces or (ii) local max utility
    public void generateTransferRequests() {

        Network.info("generating transfer requests at " + toString());
        // for each port, check if the local differential is greater
        // than the transmitted differential. 
        for (Port port : node.ports.values()) {
            if (port.type == Port.PORT_TYPE_HOST) {
                // skip over HOST ports
                continue;
            }
            port.generateTransferRequests(this);
        }

    }

    public VNF getVnf() {
        return vnf;
    }

    public int getVnfId() {
        return vnf.getVnfId();
    }

    public static class VNFNodeComparator implements Comparator {

        public int compare(Object o1, Object o2) {
            VNFNode vnode1 = (VNFNode) o1;
            VNFNode vnode2 = (VNFNode) o2;

            return vnode2.getMaxUtility() - vnode1.getMaxUtility();
        }
    }

}
