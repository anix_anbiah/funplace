/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;


/**
 *
 * @author anix
 */
public class FlowNode {

    Flow flow;  // the flow of which this is a node

    Node node;      // the network node on which this flow node exists

    // ingress port for this flow path
    Port ingressPort;

    // egress port for this flow path
    Port egressPort;
    

    // FlowPath flowPath;   // the end-to-end flow path
    SFC sfc; // the SFC for this flow

    // indices (markers) indicating the segment of the SFC
    // hosted on this node. VNF instances between the prev and next markers
    // are on this node
    int previousIndex;
    int nextIndex;

    VNF usVnf;
    int usDifferential; // upstream differential
    int usMaxUtility;
    int usDiffNodeId;   // Node ID of source of the u/s differential
    boolean usOfferPending;
    double usOfferTimestamp; // time when offer was initiated

    VNF dsVnf;
    int dsDifferential; // downstream differential
    int dsMaxUtility;
    int dsDiffNodeId;   // Node ID if the source of the d/s differential
    boolean dsOfferPending;
    double dsOfferTimestamp; // time when offer was initiated

    public FlowNode(Node localNode, Flow flow, Port ingress, Port egress) {

        this.node = localNode;
        this.flow = flow;

        this.ingressPort = ingress;
        this.egressPort = egress;
        
        this.usOfferPending = false;
        this.usOfferTimestamp = 0.0;

        this.dsOfferPending = false;
        this.dsOfferTimestamp = 0.0;

        // start and end index of VNFs are undefined initially.
        this.previousIndex = SFC.BEGIN_MARKER;
        this.nextIndex = SFC.END_MARKER;

        this.sfc = flow.getSFC();

    }

    public Flow getFlow() {
        return flow;
    }

    public int getPreviousIndex() {
        return previousIndex;
    }

    public int getNextIndex() {
        return nextIndex;
    }

    public void setPreviousIndex(int previousIndex) {
        this.previousIndex = previousIndex;
    }

    public void setNextIndex(int nextIndex) {
        this.nextIndex = nextIndex;
    }
    

    public void clearForces() {
        //clear all the forces active on this flow node.
    }

    private boolean isSfcFragmentEmpty() {
        if ((nextIndex == sfc.getFirstIndex())
                || (previousIndex == sfc.getLastIndex())
                || (nextIndex == sfc.getNextIndex(previousIndex))) {
            return true;
        }

        return false;
    }

    public int findSfcFragmentSize() {
        int startIndex, endIndex;

        if (isSfcFragmentEmpty()) {
            return 0;
        }
        // else
        startIndex = sfc.getNextIndex(previousIndex);
        endIndex = sfc.getPreviousIndex(nextIndex);

        return (endIndex - startIndex + 1);
    }

    public void removeFlowFromPushPullMap(boolean ingress, boolean pull) {

        int startIndex, endIndex;

        info("removeFlowFromPushPullMap : " + toString());

        if (ingress && pull && (previousIndex != SFC.BEGIN_MARKER)) {
            ingressPort.removeFlowIngressPullMap(this, sfc.getVnfByIndex(previousIndex));
        }

        if (!ingress && pull && (nextIndex != SFC.END_MARKER)) {
            egressPort.removeFlowEgressPullMap(this, sfc.getVnfByIndex(nextIndex));
        }

        // ingress and egress cases of pull have been handled
        if (pull) {
            return;
        }

        // now hanlde the push cases-
        startIndex = sfc.getNextIndex(previousIndex);
        endIndex = sfc.getPreviousIndex(nextIndex);
        if (!isSfcFragmentEmpty()) {
            // this means that the local SFC fragment is NOT empty

            if (ingress) {
                ingressPort.removeFlowIngressPushMap(this, sfc.getVnfByIndex(startIndex));
            }

            if (!ingress) {
                egressPort.removeFlowEgressPushMap(this, sfc.getVnfByIndex(endIndex));
            }
        } else {
            warning("SFC Fragment empty for PUSH");
        }

    }

    // addFlowToPushPullMap - assumes that previous and next indices have already been adjustd
    public void addFlowToPushPullMap(boolean ingress, boolean pull) {

        int startIndex, endIndex;

        info("addFlowToPushPullMap : " + toString());

        if (ingress && pull && (previousIndex != SFC.BEGIN_MARKER)) {
            ingressPort.addFlowIngressPullMap(this, sfc.getVnfByIndex(previousIndex));
            return;
        }

        if (!ingress && pull && (nextIndex != SFC.END_MARKER)) {
            egressPort.addFlowEgressPullMap(this, sfc.getVnfByIndex(nextIndex));
            return;
        }

        // both ingress and egress pull cases have now been handled
        if (pull) {
            return;
        }

        // now hanlde the push cases
        startIndex = sfc.getNextIndex(previousIndex);
        endIndex = sfc.getPreviousIndex(nextIndex);

        if ((nextIndex != sfc.getFirstIndex())
                && previousIndex != sfc.getLastIndex()) {

            // this means that the local SFC fragment is NOT empty
            if (ingress) {
                ingressPort.addFlowIngressPushMap(this, sfc.getVnfByIndex(startIndex));
            }

            if (!ingress) {
                egressPort.addFlowEgressPushMap(this, sfc.getVnfByIndex(endIndex));
            }
        }
    }

    // populate the push & pull maps of the ingress
    // and egress ports
    public void addFlowToPushPullMaps() {

        addFlowToPushPullMap(true, true); // INGRESS PULL

        addFlowToPushPullMap(false, true); // EGRESS PULL

        addFlowToPushPullMap(true, false); // INGRESS PUSH

        addFlowToPushPullMap(false, false); // EGRESS PUSH

    }

    public void pushVnf(VNF vnf, boolean ingress) {

        if (ingress) {

            removeFlowFromPushPullMap(true /* ingress? */, true /* pull? */); // Remove from Ingress Pull Map

            removeFlowFromPushPullMap(true, false); // Remove from Ingress Push Map

            // if we are pushing the last VNF, push from the egress as well
            if (findSfcFragmentSize() == 1) {
                removeFlowFromPushPullMap(false, false);
            }

            // we're giving up the first VNF hosted locally
            // to the upstream neighbor
            // advance the previous Index by one position (move "right")
            previousIndex = flow.getSFC().getNextIndex(previousIndex);

            addFlowToPushPullMap(true, true); // Add to Ingress Pull map

            if (!isSfcFragmentEmpty()) {

                addFlowToPushPullMap(true, false); // Add to Ingress Push map
            }

        } else {

            removeFlowFromPushPullMap(false /* ingress? */, true /* pull? */); // Remove from Egress Pull Map

            removeFlowFromPushPullMap(false, false); // Remove from Egress Push Map

            // if we are pushing the last VNF, remove from the ingress push map as well
            if (findSfcFragmentSize() == 1) {
                removeFlowFromPushPullMap(true, false);
            }

            // we are pushing the last VNF hosted locally
            // to the downstream neighbor
            // set back the next Index by one position (move "left")
            nextIndex = flow.getSFC().getPreviousIndex(nextIndex);

            addFlowToPushPullMap(false, true); // Add to Egress pull map

            if (!isSfcFragmentEmpty()) {

                addFlowToPushPullMap(false, false); // Add to Egress push map
            }
        }
        
        //INFO
        warning("PUSHED VNF " + vnf.toString() + " to "
                + (ingress ? "upstream " : "downstream ") + "node "
                + "for flow " + toString() + "; New PREV index is " + previousIndex
                + "; New NEXT index is " + nextIndex);
    }

    public void pullVnf(VNF vnf, boolean ingress) {

        if (ingress) {

            removeFlowFromPushPullMap(true, true); // Remove from Ingress Pull Map

            // TBD Do the following only if SFC fragment is not currently empty
            removeFlowFromPushPullMap(true, false); // Remove from Ingress Push Map

            // we are pulling a VNF from the ingress
            // move the previous index "left" by one position
            previousIndex = flow.getSFC().getPreviousIndex(previousIndex);

            addFlowToPushPullMap(true, true); // Add to Ingress Pull map

            addFlowToPushPullMap(true, false); // Add to Ingress Push map

        } else {

            removeFlowFromPushPullMap(false, true); // Remove from Egress Pull Map

            removeFlowFromPushPullMap(false, false); // Remove from Egress Push Map

            // we are pulling a VNF from the egress
            // move the next index "right" by one position
            nextIndex = flow.getSFC().getNextIndex(nextIndex);

            addFlowToPushPullMap(false, true); // Add to Egress Pull map

            addFlowToPushPullMap(false, false); // Add to Egress Push map
        }

        //INFO
        warning("PULLED VNF " + vnf.toString() + " FROM "
                + (ingress ? "upstream " : "downstream ") + "node "
                + "for flow " + toString()
                + "; New PREV index = " + previousIndex
                + "; New NEXT index = " + nextIndex);
    }

    public void moveSfcMarkersUpstream() {
        previousIndex = sfc.getPreviousIndex(previousIndex);
        nextIndex = sfc.getPreviousIndex(nextIndex);
    }

    public void moveSfcMarkersDownstream() {
        previousIndex = sfc.getNextIndex(previousIndex);
        nextIndex = sfc.getNextIndex(nextIndex);
    }

    public int getLoadIncrease(Message pushRequest) {
        // TBD
        // get the direction of the request from the msg
        // based on the functionIndex of the message, 
        // get the list of functions being pushed to this node
        // calculate the sum of the loads of the functions
        // and return it.
        return 1;
    }

    public int generatePull() {
        // decide on whether to pull from upstream or downstream.
        // estimate the utility of pulling a function on this flowpath.
        // consider all the other flowpaths that need the function-
        // while calculating the utility.
        // generate pull messages for all the flowpaths that need this function
        // add the messages to the list of outgoing messages.

        return 0;
    }

//    public int reduceLoad(int savingsTarget) {
//        if (endIndex <= startIndex) {
//            // none of the functions are implemented locally
//            return 0;
//        }
//
//        if (upstreamPushReceived && downstreamPushReceived) {
//            // we have received push from both sides, we cannot push 
//            // in either direction
//            return 0;
//        }
//
//        if (upstreamPushReceived) {
//            // push to downstream node
//            // consider the functions in reverse, starting from endIndex
//            // collect as many functions as needed to meet savingsTarget
//            // create a pushRequest message and add it to list of outgoing
//            // messages
//        } else {
//            // do the samething, but proceed forward from firstIndex 
//            // and push to upstream node
//            return 0;
//        }
//
//        return 0;
//    }

    public void utCheckVnf(VNF vnf) {
        int index = sfc.indexOf(vnf);

        if (index <= previousIndex || index >= nextIndex) {
            warning("Unit Test failed for " + toString());
        }
    }

    public void info(String msg) {
        flow.info(toString() + " " + msg);
    }

    public void warning(String msg) {
        flow.warning(toString() + " " + msg);
    }    
    public String toString() {
        return ("Flow Node: @" + node.toString() + "-Flow" + flow.getFlowId() + " ");
    }

}
