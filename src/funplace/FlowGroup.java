/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author anix
 */
public class FlowGroup {
    
    List<Flow> flows;         // 4 bytes per flow on the wire
    int        differential;  // Not needed for wire
    int        maxUtility;    // Not needed for wire
    // aggregate bit rate of all flows
    int        totalBitRate; // Not needed for wire
    // TOTAL Byte size on the wire = # of flows x 4 bytes
    
    
    public FlowGroup() {
        flows = new ArrayList<>();
        differential = 0;
    }

    public List<Flow> getFlows() {
        return flows;
    }

//    public void setFlows(List<Flow> flows) {
//        this.flows = flows;
//    }

    public int getDifferential() {
        return differential;
    }

    public void setDifferential(int differential) {
        this.differential = differential;
    }
    
    public void addFlow(Flow flow) {
        flows.add(flow);
    }
    
    public void addFlows(Collection toBeAdded) {
        flows.addAll(toBeAdded);
    }
    
    
}
