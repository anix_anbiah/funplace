/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.*;
import org.jgrapht.graph.*;

/**
 *
 * @author anix
 */
public class Flow {

    // the list of nodes that carry the flow
    List<Node> nodes;
    GraphPath<Node, Link> path;
    SFC sfc; // service chain for this flow

    int bitRate;
    int flowId;

    private final static Logger logger = Logger.getLogger(Flow.class.getName());

    boolean debug;

    public Flow(int bitRate, int flowId, SFC sfc, GraphPath<Node, Link> path) {

        this.bitRate = bitRate;
        this.flowId = flowId;
        this.sfc = sfc;

        this.path = path;

        logger.setLevel(Network.defaultLogLevel);
        this.debug = false;
    }

    public GraphPath<Node, Link> getPath() {
        return path;
    }
    
    public SFC getSFC() {
        return sfc;
    }

    public int getBitRate() {
        return bitRate;
    }

    public int getFlowId() {
        return flowId;
    }

    public String toString() {
        return "F" + getFlowId();
    }

    public void setDebugFlag(boolean val) {
        this.debug = val;
    }

    public void info(String msg) {
        if (logger.isLoggable(Level.INFO) || debug) {
            System.out.println(msg);
        }
    }

    public void warning(String msg) {
        if (logger.isLoggable(Level.WARNING)) {
            System.out.println("WARNING: " + msg);
        }
    }
    
    public void prettyPrint() {
        FlowNode fnode;

        System.out.println(toString());
        System.out.println("PATH :: " + path.toString());

        System.out.print("FLOW NODES :: ");
        for (Node n : path.getVertexList()) {
            fnode = n.getFlowNodeByFlowId(flowId);

            System.out.print(fnode.node.toString() + " ");

            System.out.print("[VNFs :: (" + SFC.indexToString(fnode.previousIndex) + ","
                    + SFC.indexToString(fnode.nextIndex) + ") ");
            sfc.prettyPrint(fnode.previousIndex, fnode.nextIndex);
            System.out.print("] ");
        }

        System.out.println();
    }

}
