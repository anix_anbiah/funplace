/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author anix
 */
public class Message {
    
    public static class Type {
        static final int TRIGGER=99;
        static final int REQUEST=1;
        static final int OFFER=2;
        static final int CONFIRM=3;
        
        public static String toString(int type) {
            if(type == TRIGGER) {
                return "TRIGGER";
            } else if(type == REQUEST) {
                return "REQUEST";
            } else if(type == OFFER) {
                return "OFFER";
            } else if(type == CONFIRM) {
                return "CONFIRM";
            }
            
            return "UNKNOWN";
        }
    }
   

    // Add 4 bytes for Type & Length header for the message
    int type;  // 4 bytes on the wire
    VNF vnf;   // 4 bytes
    int srcNodeId; // 4 bytes
    int dstNodeId; // 4 bytes
    int maxUtility; // 4 bytes
    
    // differential in utility, +ve for pull, -ve for push
    int differential; // 4 bytes
    FlowGroup flowGroup;  // # of flows x 4 Bytes
    
    // link over which this the message is sent
    Link link; // This is not needed in the wire format
    // TOTAL length on the wire-  44 + # of flows x 4 bytes
    
    private final int MSG_SIZE_FIXED = 24;
  
    
    public Message(int type, VNF vnf) {
        this.type = type;
        this.vnf = vnf;
        
        this.flowGroup = new FlowGroup();
    }
    

    
//    public void accept() {
//        // accept a request 
//        if(type.equals(Type.PULL_REQUEST)) {
//            // this is a pull request 
//            // check if it is from upstream or downstream node
//            if(dir == FlowPath.Dir.UPSTREAM) {
//                // functions from firstIndex to functionIndex for
//                // flow path identified by flowPathId should be 
//                // stopped in the local node and
//                // PULL_RESPONSE message must be added to list
//                // of outgoing messages
//                // TBD
//            } else {
//                // do the same thing, but from functionIndex to lastIndex
//            }
//        }
//    }
//    
//    public void reject() {
//        if(type.equals(Type.PULL_REQUEST)) {
//            // create a PULL_REJECT and add to outgoing messages
//        } else {
//            // create a PUSH_REJECT and add to outgoing messages
//        }
//    }

    public FlowGroup getFlowGroup() {
        return flowGroup;
    }
    
    public String toString() {
        String flows = "";
        for(Flow fl:flowGroup.getFlows()) {
            flows += fl.toString() + " ";
        }
        return "Message " + Type.toString(type) + "; VNF " + vnf.toString() +
                "; From N" + srcNodeId + "; To " + 
                (dstNodeId == 0 ? "ALL" : ("N"+dstNodeId)) + "; received over " +
                ((link == null) ? " Null link " : link.toString()) + " differential = " + differential +
                "; Max Utility = " + maxUtility + "; Num flows = " +
                flowGroup.getFlows().size() + " Flows: " + flows; 
    }
    
    public int sizeOf() {
        // size of a message is 52 bytes fixed + 4 x number of flows
        return MSG_SIZE_FIXED + flowGroup.getFlows().size() * 4;
    }
    
}
