/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

import java.util.List;
import java.util.ArrayList;
import eduni.simjava.Sim_system;
import static funplace.Network.warning;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author anix
 */
public class Controller {

    static private Controller controller = null;
    public static final int MAX_NETWORKS = 1;
    public static final int ROUNDS_PER_NETWORK = 200;
    static protected String OUTPUT_DIR = "";

    public static double DEFAULT_VNF_MIGRATION_COST = 0.0; // cost of migrating a VNF in units of # of VNFs.
    public static double DEFAULT_FLOW_DIST_COEFF = 1.0; // flow size distribution coefficient

    List<Node> nodes;
    List<Function> functions;

    private static PrintWriter writer;

    // Private constructor for singleton class Controller
    private Controller() {
        nodes = new ArrayList<>();
    }

    public Controller getInstance() {
        if (controller == null) {
            controller = new Controller();
        }

        return controller;
    }

    public void init() {
        // Create all the nodes and flow paths (from property files)- TBD
        // then, call execute() to start the controller
    }

    public static void main(String[] args) throws InterruptedException {

        String topo, netName, sfcStr;
        int ftTopoK, ftTopoH, numFlows, sfcLen;
        String directoryName;

        // SADHANA
        double vnfMigrationCost;
        double fdCoeff; // coefficient used for flow size distribution
        // a value of 0.9 means 10% of the flows contribute 90% of the data rate and so on.

        // Network[] network = new Network[MAX_NETWORKS];
        Network[] net = new Network[MAX_NETWORKS];
//        int totalRoundsTaken = 0;
//        int totalInitialVnfInstances = 0;
//        int totalFinalVnfInstances = 0;
        int netId = 0;

        // Initialise Sim_system
        Sim_system.initialise();

//        Sim_system.set_output_analysis(Sim_system.IND_REPLICATIONS, 2, 0.50);
        // create the VNF definitions
        VNF.createVNFs();

        // create the SFC instances
        SFC.createSFCs();

        netName = args[0];
        topo = args[1];
        ftTopoK = Integer.parseInt(args[2]);
        ftTopoH = Integer.parseInt(args[3]);
        sfcStr = args[4];
        sfcLen = "ST".equals(sfcStr) ? SFC.SFC_LEN_3
                : ("MD".equals(sfcStr) ? SFC.SFC_LEN_5 : SFC.SFC_LEN_8);
        numFlows = Integer.parseInt(args[5]);

        vnfMigrationCost = DEFAULT_VNF_MIGRATION_COST;
        if (args.length > 6) {
            vnfMigrationCost = Double.parseDouble(args[6]);
        }

        fdCoeff = DEFAULT_FLOW_DIST_COEFF;
        if (args.length > 7) {
            fdCoeff = Double.parseDouble(args[7]);
            directoryName = OUTPUT_DIR + topo + "/" + fdCoeff + "D/"
                    + sfcStr + "/" + numFlows + "F/";
        } else {

            directoryName = OUTPUT_DIR + topo + "/" + vnfMigrationCost + "M/"
                    + sfcStr + "/" + numFlows + "F/";
        }

        File directory = new File(String.valueOf(directoryName));

        if (!directory.exists()) {
            System.out.println("Creating directory " + directoryName);
            directory.mkdirs();
            if (!directory.exists()) {
                System.out.println("Directory did not get created");
            }
        }

//        try {
//            writer = new PrintWriter(directoryName
//                    + "Aggr" + ".dat", "ASCII");
//        } catch (IOException e) {
//            warning("Unable to open dump file for writing: " + e.getMessage());
//            return;
//        }
        boolean sfcHetero = false;

        //boolean ecmp = "bcube".equals(topo) ? false : true;
        boolean ecmp = false;

        System.out.println("Creating Network for " + topo + " topology. ECMP = " + ecmp);

        for (netId = 0; netId < MAX_NETWORKS; netId++) {
            net[netId] = new Network(netName, topo, ftTopoK, ftTopoH, directoryName,
                    numFlows, netId * ROUNDS_PER_NETWORK + 1, (netId + 1) * ROUNDS_PER_NETWORK,
                    sfcLen, /* sfc len */ sfcHetero, ecmp, vnfMigrationCost, fdCoeff);
        }

        // find the averages and dump them in the output file
        Sim_system.run();

        for (netId = 0; netId < MAX_NETWORKS; netId++) {

//            totalRoundsTaken += net[netId].roundsTaken();
//            totalInitialVnfInstances += net[netId].initialVnfInstances();
//            totalFinalVnfInstances += net[netId].finalVnfInstances();
            System.out.println("Stats for Net " + netId
                    + "; Rounds = " + net[netId].roundsTaken()
                    + "; Initial VNFs = " + net[netId].initialVnfInstances()
                    + "; Final VNFs = " + net[netId].finalVnfInstances());

        }

//        double avgRounds = ((double) totalRoundsTaken) / MAX_NETWORKS;
//        double avgInitialVnfs = ((double) totalInitialVnfInstances) / MAX_NETWORKS;
//        double avgFinalVnfs = ((double) totalFinalVnfInstances) / MAX_NETWORKS;
//        dumpln("# FT Topology K = " + topoArg1 + "; H = " + topoArg2);
//        dumpln("# SFC Len = " + sfcLen + ": "
//                + (sfcHetero ? "HETERO" : "HOMO"));
//        dumpln("# Num of Flows = " + numFlows);
//        dumpln("# AVG Num of Rounds taken = " + avgRounds);
//        dumpln("# AVG Num of VNF instances = "
//                + avgInitialVnfs + " (Initial) "
//                + avgFinalVnfs + " (Final)");
//        dumpln("");
//        dumpln("");
//
//        writer.close();
    }

    private static void dumpln(String str) {
        writer.println(str);
    }
}
