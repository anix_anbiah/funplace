/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funplace;

/**
 *
 * @author anix
 */
public class VNFName {
    
    public static final int STAR = 1;
    public static final int SQUARE = 2;
    public static final int CIRCLE = 3;
    public static final int TRIANGLE = 4;
    public static final int DIAMOND = 5;
    public static final int HEART = 6;
    public static final int SPADE = 7;
    public static final int CLOVER = 8;
    public static final int OVAL = 9;
    public static final int RING = 10;
    public static final int RHOMBUS = 11;
    public static final int RECTANBLE = 12;
    
    public static final String[] VNF_NAME= {"ZERO", "STAR", "SQUARE", "CIRCLE",
        "TRIANGLE", "DIAMOND", "HEART", "SPADE", "CLOVER", "OVAL", "RING", 
        "RHOMBUS", "RECTANGLE"
    };
    
    public static String vnfName(int vnfId) {
        return VNF_NAME[vnfId];
    }
}
